--
-- Copyright (c) 2006 Institut de recherches cliniques de Montreal (IRCM)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.
--

DROP TABLE IF EXISTS organism;
DROP TABLE IF EXISTS taxon;

CREATE TABLE organism (
  id BIGINT(10) NOT NULL,
  name varchar(255) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE (name)
);
INSERT INTO organism (id, name) VALUES
(9606, 'Homo Sapiens'),
(10090, 'Mus Musculus');

CREATE TABLE taxon (
  id BIGINT(10) NOT NULL,
  name varchar(255),
  rank varchar(255),
  parentId BIGINT(10),
  PRIMARY KEY (id),
  FOREIGN KEY (parentId) REFERENCES taxon (id) ON DELETE CASCADE ON UPDATE CASCADE
);
INSERT INTO taxon (id, name, rank, parentId) VALUES (1, 'root', 'no rank', null);
INSERT INTO taxon (id, name, rank, parentId) VALUES (131567, 'cellular organisms', 'no rank', 1);
INSERT INTO taxon (id, name, rank, parentId) VALUES (2759, 'Eukaryota', 'superkingdom', 131567);
INSERT INTO taxon (id, name, rank, parentId) VALUES (33154, 'Opisthokonta', 'no rank', 2759);
INSERT INTO taxon (id, name, rank, parentId) VALUES (4751, 'Fungi', 'kingdom', 33154);
INSERT INTO taxon (id, name, rank, parentId) VALUES (451864, 'Dikarya', 'subkingdom', 4751);
INSERT INTO taxon (id, name, rank, parentId) VALUES (4890, 'Ascomycota', 'phylum', 451864);
INSERT INTO taxon (id, name, rank, parentId) VALUES (716545, 'saccharomyceta', 'no rank', 4890);
INSERT INTO taxon (id, name, rank, parentId) VALUES (147537, 'Saccharomycotina', 'subphylum', 716545);
INSERT INTO taxon (id, name, rank, parentId) VALUES (4891, 'Saccharomycetes', 'class', 147537);
INSERT INTO taxon (id, name, rank, parentId) VALUES (4892, 'Saccharomycetales', 'order', 4891);
INSERT INTO taxon (id, name, rank, parentId) VALUES (4893, 'Saccharomycetaceae', 'family', 4892);
INSERT INTO taxon (id, name, rank, parentId) VALUES (4930, 'Saccharomyces', 'genus', 4893);
INSERT INTO taxon (id, name, rank, parentId) VALUES (4932, 'Saccharomyces cerevisiae', 'species', 4930);
INSERT INTO taxon (id, name, rank, parentId) VALUES (1337652, 'Saccharomyces cerevisiae 101S', 'no rank', 4930);
INSERT INTO taxon (id, name, rank, parentId) VALUES (559292, 'Saccharomyces cerevisiae S288c', 'no rank', 4930);
INSERT INTO taxon (id, name, rank, parentId) VALUES (580240, 'Saccharomyces cerevisiae W303', 'no rank', 4930);
INSERT INTO taxon (id, name, rank, parentId) VALUES (33208, 'Metazoa', 'kingdom', 33154);
INSERT INTO taxon (id, name, rank, parentId) VALUES (6072, 'Eumetazoa', 'no rank', 33208);
INSERT INTO taxon (id, name, rank, parentId) VALUES (33213, 'Bilateria', 'no rank', 6072);
INSERT INTO taxon (id, name, rank, parentId) VALUES (33511, 'Deuterostomia', 'no rank', 33213);
INSERT INTO taxon (id, name, rank, parentId) VALUES (7711, 'Chordata', 'phylum', 33511);
INSERT INTO taxon (id, name, rank, parentId) VALUES (89593, 'Craniata', 'subphylum', 7711);
INSERT INTO taxon (id, name, rank, parentId) VALUES (7742, 'Vertebrata', 'no rank', 89593);
INSERT INTO taxon (id, name, rank, parentId) VALUES (7776, 'Gnathostomata', 'no rank', 7742);
INSERT INTO taxon (id, name, rank, parentId) VALUES (117570, 'Teleostomi', 'no rank', 7776);
INSERT INTO taxon (id, name, rank, parentId) VALUES (117571, 'Euteleostomi', 'no rank', 117570);
INSERT INTO taxon (id, name, rank, parentId) VALUES (8287, 'Sarcopterygii', 'no rank', 117571);
INSERT INTO taxon (id, name, rank, parentId) VALUES (1338369, 'Dipnotetrapodomorpha', 'no rank', 8287);
INSERT INTO taxon (id, name, rank, parentId) VALUES (32523, 'Tetrapoda', 'no rank', 1338369);
INSERT INTO taxon (id, name, rank, parentId) VALUES (32524, 'Amniota', 'no rank', 32523);
INSERT INTO taxon (id, name, rank, parentId) VALUES (40674, 'Mammalia', 'class', 32524);
INSERT INTO taxon (id, name, rank, parentId) VALUES (32525, 'Theria', 'no rank', 40674);
INSERT INTO taxon (id, name, rank, parentId) VALUES (9347, 'Eutheria', 'no rank', 32525);
INSERT INTO taxon (id, name, rank, parentId) VALUES (1437010, 'Boreoeutheria', 'no rank', 9347);
INSERT INTO taxon (id, name, rank, parentId) VALUES (314146, 'Euarchontoglires', 'superorder', 1437010);
INSERT INTO taxon (id, name, rank, parentId) VALUES (9443, 'Primates', 'order', 314146);
INSERT INTO taxon (id, name, rank, parentId) VALUES (376913, 'Haplorrhini', 'suborder', 9443);
INSERT INTO taxon (id, name, rank, parentId) VALUES (314293, 'Simiiformes', 'infraorder', 376913);
INSERT INTO taxon (id, name, rank, parentId) VALUES (9526, 'Catarrhini', 'parvorder', 314293);
INSERT INTO taxon (id, name, rank, parentId) VALUES (314295, 'Hominoidea', 'superfamily', 9526);
INSERT INTO taxon (id, name, rank, parentId) VALUES (9604, 'Hominidae', 'family', 314295);
INSERT INTO taxon (id, name, rank, parentId) VALUES (207598, 'Homininae', 'subfamily', 9604);
INSERT INTO taxon (id, name, rank, parentId) VALUES (9605, 'Homo', 'genus', 207598);
INSERT INTO taxon (id, name, rank, parentId) VALUES (9606, 'Homo sapiens', 'species', 9605);
