/*
 * Copyright (c) 2006 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.pdd;

import static org.junit.Assert.assertEquals;

import java.nio.file.Paths;
import org.junit.Test;

public class StringToPathConverterTest {
  private StringToPathConverter pathConverter = new StringToPathConverter();

  @Test
  public void convert() {
    assertEquals(Paths.get("test.txt"), pathConverter.convert("test.txt"));
    assertEquals(Paths.get("/usr/local/test.txt"), pathConverter.convert("/usr/local/test.txt"));
    assertEquals(Paths.get("c:\\test.txt"), pathConverter.convert("c:\\test.txt"));
    assertEquals(Paths.get("${user.home}/test.txt"),
        pathConverter.convert("${user.home}/test.txt"));
  }
}
