/*
 * Copyright (c) 2006 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.pdd.test;

import ca.qc.ircm.pdd.taxonomy.Taxon;
import java.io.BufferedWriter;
import java.io.LineNumberReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Creates database-insert-taxons.sql containing all taxons in nodes.dmp and names.dmp.
 * <p>
 * Taxons are taken from ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/taxdmp.zip.
 * </p>
 */
public class CreateInsetTaxons {
  private static final String SCIENTIFIC_NAME = "scientific name";

  /**
   * Creates database-insert-taxons.sql containing all taxons in nodes.dmp and names.dmp.
   *
   * @param args
   *          not used
   * @throws Exception
   *           any exception that occurs
   */
  public static void main(String[] args) throws Exception {
    Map<Integer, Taxon> taxons = new HashMap<>();
    try (LineNumberReader reader = new LineNumberReader(Files.newBufferedReader(
        Paths.get(CreateInsetTaxons.class.getClass().getResource("/nodes.dmp").toURI())))) {
      String line;
      while ((line = reader.readLine()) != null) {
        String[] columns = line.split("\\t");
        Integer id = Integer.parseInt(columns[0]);
        Taxon taxon = new Taxon(id);
        taxon.setParent(new Taxon(Integer.parseInt(columns[2])));
        if (taxon.getParent().getId() == taxon.getId()) {
          taxon.setParent(null);
        }
        taxon.setRank(columns[4]);
        taxons.put(id, taxon);
      }
    }
    try (LineNumberReader reader = new LineNumberReader(Files.newBufferedReader(
        Paths.get(CreateInsetTaxons.class.getClass().getResource("/names.dmp").toURI())))) {
      String line;
      while ((line = reader.readLine()) != null) {
        String[] columns = line.split("\\t");
        Integer id = Integer.parseInt(columns[0]);
        Taxon taxon = taxons.get(id);
        if (taxon.getName() == null || columns[6].equals(SCIENTIFIC_NAME)) {
          taxon.setName(columns[2]);
        }
      }
    }
    Map<Integer, Integer> orders = new HashMap<>();
    taxons.values().forEach(t -> orders.put(t.getId(), order(t, taxons)));
    List<Taxon> sortedTaxon = taxons.values().stream()
        .sorted((t1, t2) -> orders.get(t1.getId()).compareTo(orders.get(t2.getId())))
        .collect(Collectors.toList());
    try (BufferedWriter writer = Files.newBufferedWriter(Paths
        .get(System.getProperty("user.dir") + "/src/test/resources/database-insert-taxons.sql"))) {
      for (int i = 0; i < sortedTaxon.size(); i++) {
        Taxon taxon = sortedTaxon.get(i);
        writer.write("MERGE INTO taxon (id, name, uniqueName, rank, parentId) VALUES (");
        writer.write(String.valueOf(taxon.getId()));
        writer.write(", '");
        writer.write(taxon.getName().replace("'", "''"));
        writer.write("', '");
        writer.write(taxon.getRank().replace("'", "''"));
        writer.write("', ");
        writer
            .write(taxon.getParent() != null ? String.valueOf(taxon.getParent().getId()) : "null");
        writer.write(")");
        writer.write(";");
        writer.write("\n");
      }
    }
  }

  private static int order(Taxon taxon, Map<Integer, Taxon> taxons) {
    if (taxon.getParent() == null) {
      return 0;
    } else {
      return 1 + order(taxons.get(taxon.getParent().getId()), taxons);
    }
  }
}
