/*
 * Copyright (c) 2006 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.pdd.gui;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import ca.qc.ircm.pdd.test.config.TestFxTestAnnotations;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.function.Consumer;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.stage.Window;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.testfx.framework.junit.ApplicationTest;

@RunWith(SpringJUnit4ClassRunner.class)
@TestFxTestAnnotations
public class TaskStarterTest extends ApplicationTest {
  private TaskStarter taskStarter;
  @Mock
  private ExecutorService executorService;
  @Mock
  private Consumer<String> successConsumer;
  @Mock
  private Consumer<String> successConsumer2;
  @Mock
  private Consumer<Throwable> failConsumer;
  @Mock
  private Consumer<Throwable> failConsumer2;
  @Mock
  private EventHandler<WorkerStateEvent> eventHandler;
  private String successMessage = "success";
  private Task<String> successTask = new Task<String>() {
    @Override
    protected String call() throws Exception {
      return successMessage;
    }
  };
  private Exception failException = new IOException("failed");
  private Task<String> failTask = new Task<String>() {
    @Override
    protected String call() throws Exception {
      throw failException;
    }
  };
  private Window window;

  @Before
  public void beforeTest() {
    taskStarter = new TaskStarter(executorService);
  }

  @Override
  public void start(Stage stage) throws Exception {
    Scene scene = new Scene(new Label("test"));
    stage.setScene(scene);
    window = scene.getWindow();
  }

  @Test
  public void start() throws Throwable {
    interact(() -> taskStarter.build(window, successTask).start());

    Window window = listWindows().get(0);
    assertNotNull(from(rootNode(window)).lookup(".progress-dialog").query());
    failTask.run();
    Thread.sleep(1000);
    verify(executorService).submit(successTask);
  }

  @Test
  public void apply() throws Throwable {
    interact(() -> taskStarter.build(window, successTask).apply(t -> t.setOnSucceeded(eventHandler))
        .start());

    successTask.run();
    Thread.sleep(1000);
    verify(eventHandler).handle(any());
  }

  @Test
  public void onSuccess() throws Throwable {
    interact(() -> taskStarter.build(window, successTask).onSuccess(successConsumer).start());

    successTask.run();
    Thread.sleep(1000);
    verify(successConsumer).accept(successMessage);
  }

  @Test
  public void onSuccess_Multiple() throws Throwable {
    interact(() -> taskStarter.build(window, successTask).onSuccess(successConsumer)
        .onSuccess(successConsumer2).start());

    successTask.run();
    Thread.sleep(1000);
    verify(successConsumer, never()).accept(successMessage);
    verify(successConsumer2).accept(successMessage);
  }

  @Test
  public void onFail() throws Throwable {
    interact(() -> taskStarter.build(window, failTask).onFail(failConsumer).start());

    failTask.run();
    Thread.sleep(1000);
    verify(failConsumer).accept(failException);
  }

  @Test
  public void onFail_Multiple() throws Throwable {
    interact(() -> taskStarter.build(window, failTask).onFail(failConsumer).onFail(failConsumer2)
        .start());

    failTask.run();
    Thread.sleep(1000);
    verify(failConsumer, never()).accept(failException);
    verify(failConsumer2).accept(failException);
  }
}
