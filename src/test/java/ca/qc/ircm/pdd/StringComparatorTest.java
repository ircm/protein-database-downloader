/*
 * Copyright (c) 2006 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.pdd;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class StringComparatorTest {
  private StringComparator comparator = new StringComparator();

  @Test
  public void compare() {
    assertTrue(comparator.compare("abc", "def") < 0);
    assertTrue(comparator.compare("abc", "abc") == 0);
    assertTrue(comparator.compare("def", "abc") > 0);
    assertTrue(comparator.compare("def", "ghi") < 0);
    assertTrue(comparator.compare("def", "def") == 0);
    assertTrue(comparator.compare("ghi", "abc") > 0);
    assertTrue(comparator.compare("abc", "ABC") > 0);
    assertTrue(comparator.compare("ABC", "abc") < 0);
  }

  @Test
  public void compare_Case() {
    comparator = new StringComparator(false);
    assertTrue(comparator.compare("abc", "def") < 0);
    assertTrue(comparator.compare("abc", "abc") == 0);
    assertTrue(comparator.compare("def", "abc") > 0);
    assertTrue(comparator.compare("def", "ghi") < 0);
    assertTrue(comparator.compare("def", "def") == 0);
    assertTrue(comparator.compare("ghi", "abc") > 0);
    assertTrue(comparator.compare("abc", "ABC") > 0);
    assertTrue(comparator.compare("ABC", "abc") < 0);
  }

  @Test
  public void compare_IgnoreCase() {
    comparator = new StringComparator(true);
    assertTrue(comparator.compare("abc", "def") < 0);
    assertTrue(comparator.compare("abc", "abc") == 0);
    assertTrue(comparator.compare("def", "abc") > 0);
    assertTrue(comparator.compare("def", "ghi") < 0);
    assertTrue(comparator.compare("def", "def") == 0);
    assertTrue(comparator.compare("ghi", "abc") > 0);
    assertTrue(comparator.compare("abc", "ABC") == 0);
    assertTrue(comparator.compare("ABC", "abc") == 0);
  }
}
