/*
 * Copyright (c) 2006 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.pdd.download;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import ca.qc.ircm.pdd.annotation.SequenceConfiguration;
import ca.qc.ircm.pdd.annotation.UniprotConfiguration;
import ca.qc.ircm.pdd.ftp.FtpService;
import ca.qc.ircm.pdd.organism.Organism;
import ca.qc.ircm.pdd.taxonomy.DownloadTaxonomyService;
import ca.qc.ircm.pdd.taxonomy.Taxon;
import ca.qc.ircm.pdd.test.config.ServiceTestAnnotations;
import ca.qc.ircm.progressbar.ProgressBar;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.zip.GZIPOutputStream;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ServiceTestAnnotations
public class DownloadServiceUniprotTest {
  private static final Charset FASTA_CHARSET = Charset.forName("UTF-8");
  private static final String CAPITAL_LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  private static final String NUMBERS = "0123456789";
  private DownloadServiceUniprot downloadServiceUniprot;
  @Mock
  private SequenceConfiguration sequenceConfiguration;
  @Mock
  private FtpService ftpService;
  @Mock
  private DownloadTaxonomyService downloadTaxonomyService;
  @Mock
  private UniprotConfiguration uniprotConfiguration;
  @Mock
  private Organism organism;
  @Mock
  private ProgressBar progressBar;
  @Mock
  private FTPClient ftpClient;
  @Mock
  private Map<Integer, Taxon> taxonomy;
  @Mock
  private Taxon taxon;
  @Rule
  public TemporaryFolder temporaryFolder = new TemporaryFolder();
  private Locale locale;
  private String ftp;
  private String swissprotFastaUrl;
  private String tremblFastaUrl;
  private String idmappingUrl;
  private String taxonomyMappingName = "NCBI_TaxID";
  private Random random = new Random();

  /**
   * Before test.
   */
  @Before
  public void beforeTest() throws Throwable {
    downloadServiceUniprot = new DownloadServiceUniprot(sequenceConfiguration,
        downloadTaxonomyService, uniprotConfiguration, ftpService);
    locale = Locale.getDefault();
    ftp = "ftp.localhost";
    swissprotFastaUrl = "/pub/swissprot.fasta.gz";
    tremblFastaUrl = "/pub/trembl.fasta.gz";
    idmappingUrl = "/pub/idmapping.gz";
    when(uniprotConfiguration.getFtp()).thenReturn(ftp);
    when(uniprotConfiguration.getSwissprotFasta()).thenReturn(swissprotFastaUrl);
    when(uniprotConfiguration.getTremblFasta()).thenReturn(tremblFastaUrl);
    when(uniprotConfiguration.getIdmapping()).thenReturn(idmappingUrl);
    when(uniprotConfiguration.getTaxonomyMappingName()).thenReturn(taxonomyMappingName);
    when(sequenceConfiguration.getDecoyName()).thenReturn("DECOY{0,number,#}");
    when(ftpService.anonymousConnect(any())).thenReturn(ftpClient);
    when(progressBar.step(anyDouble())).thenReturn(progressBar);
    doAnswer(i -> {
      System.out.println(i.getArguments()[0]);
      return null;
    }).when(progressBar).setMessage(any());
  }

  private List<String> generateUniprotAccessions(int count) {
    return IntStream.range(0, count).mapToObj(i -> generateUniprotAccession())
        .collect(Collectors.toList());
  }

  private String generateUniprotAccession() {
    StringBuilder builder = new StringBuilder();
    if (random.nextInt(2) == 0) {
      builder.append(RandomStringUtils.random(1, "OPQ"));
      builder.append(RandomStringUtils.random(1, NUMBERS));
      builder.append(RandomStringUtils.random(3, NUMBERS + CAPITAL_LETTERS));
      builder.append(RandomStringUtils.random(1, NUMBERS));
    } else {
      builder.append(RandomStringUtils.random(1, "ABCDEFGHIJKLMNRSTUVWXYZ"));
      builder.append(RandomStringUtils.random(1, NUMBERS));
      builder.append(RandomStringUtils.random(1, CAPITAL_LETTERS));
      builder.append(RandomStringUtils.random(2, NUMBERS + CAPITAL_LETTERS));
      builder.append(RandomStringUtils.random(1, NUMBERS));
      if (random.nextInt(2) == 0) {
        builder.append(RandomStringUtils.random(1, CAPITAL_LETTERS));
        builder.append(RandomStringUtils.random(2, NUMBERS + CAPITAL_LETTERS));
        builder.append(RandomStringUtils.random(1, NUMBERS));
      }
    }
    return builder.toString();
  }

  private String generateFastaSequences(List<String> ids) {
    StringBuilder sequences = new StringBuilder();
    ids.forEach(id -> {
      sequences.append(">sp|" + id + " " + RandomStringUtils.randomAlphanumeric(40));
      sequences.append("\n");
      sequences.append(RandomStringUtils.randomAlphabetic(80).toUpperCase());
      sequences.append("\n");
      sequences.append(RandomStringUtils.randomAlphabetic(40).toUpperCase());
      sequences.append("\n");
      sequences.append("\n");
    });
    return sequences.toString();
  }

  private String generateIdMapping(List<String> ids, String taxonomy) {
    StringBuilder sequences = new StringBuilder();
    ids.forEach(id -> {
      sequences.append(id);
      sequences.append("\t");
      sequences.append("UniProtKB-ID");
      sequences.append("\t");
      sequences.append(RandomStringUtils.randomAlphanumeric(10));
      sequences.append("\n");
      sequences.append(id);
      sequences.append("\t");
      sequences.append(taxonomyMappingName);
      sequences.append("\t");
      sequences.append(taxonomy);
      sequences.append("\n");
    });
    return sequences.toString();
  }

  private byte[] gzip(byte[] bytes) throws IOException {
    ByteArrayOutputStream output = new ByteArrayOutputStream();
    try (GZIPOutputStream compressedOutput = new GZIPOutputStream(output)) {
      compressedOutput.write(bytes);
    }
    return output.toByteArray();
  }

  @Test
  public void allProteinMappings_4930() throws Throwable {
    final int organismId = 4930;
    when(organism.getId()).thenReturn(organismId);
    DownloadParametersBean parameters = new DownloadParametersBean();
    parameters.organism(organism);
    parameters.proteinDatabase(ProteinDatabase.UNIPROT);
    List<Integer> taxonChildrenIds = new ArrayList<>();
    taxonChildrenIds.add(4932);
    taxonChildrenIds.add(765312);
    taxonChildrenIds.add(545124);
    taxonChildrenIds.add(1337652);
    when(downloadTaxonomyService.children(any(), any(), any()))
        .thenReturn(new HashSet<>(taxonChildrenIds));
    Map<Integer, List<String>> taxonIds = taxonChildrenIds.stream()
        .collect(Collectors.toMap(t -> t, t -> generateUniprotAccessions(20)));
    taxonIds.put(organismId, generateUniprotAccessions(20));
    List<String> otherIds = generateUniprotAccessions(100);
    final String expectedSwissprotFasta = taxonIds.values().stream()
        .map(ids -> generateFastaSequences(ids.subList(0, 5))).collect(Collectors.joining());
    final String expectedTremblFasta = taxonIds.values().stream()
        .map(ids -> generateFastaSequences(ids.subList(5, 20))).collect(Collectors.joining());
    final String otherSwissprotFasta = generateFastaSequences(otherIds.subList(0, 20));
    final String otherTremblFasta = generateFastaSequences(otherIds.subList(20, 100));
    final String swissprotFasta = otherSwissprotFasta + "\n" + expectedSwissprotFasta;
    final String tremblFasta = otherTremblFasta + "\n" + expectedTremblFasta;
    Path swissprot = temporaryFolder.getRoot().toPath().resolve("pub/swissprot.fasta.gz");
    Files.createDirectories(swissprot.getParent());
    Files.write(swissprot, gzip(swissprotFasta.getBytes(FASTA_CHARSET)));
    when(ftpService.localFile(swissprotFastaUrl)).thenReturn(swissprot);
    Path trembl = temporaryFolder.getRoot().toPath().resolve("pub/trembl.fasta.gz");
    Files.createDirectories(trembl.getParent());
    Files.write(trembl, gzip(tremblFasta.getBytes(FASTA_CHARSET)));
    when(ftpService.localFile(tremblFastaUrl)).thenReturn(trembl);
    Path idmapping = temporaryFolder.getRoot().toPath().resolve("pub/idmapping.gz");
    Files.createDirectories(idmapping.getParent());
    String idmappingContent = generateIdMapping(otherIds, "9606");
    idmappingContent += taxonIds.entrySet().stream()
        .map(e -> generateIdMapping(e.getValue(), String.valueOf(e.getKey())))
        .collect(Collectors.joining());
    Files.write(idmapping, gzip(idmappingContent.getBytes(FASTA_CHARSET)));
    when(ftpService.localFile(idmappingUrl)).thenReturn(idmapping);
    final Path output = temporaryFolder.newFile("output.fasta").toPath();

    downloadServiceUniprot.downloadProteinDatabase(parameters, output, progressBar, locale);

    verify(downloadTaxonomyService).children(organismId, progressBar, locale);
    verify(ftpService, atLeastOnce()).anonymousConnect(ftp);
    verify(ftpService).localFile(swissprotFastaUrl);
    verify(ftpService).downloadFile(ftpClient, swissprotFastaUrl, swissprot, progressBar, locale);
    verify(ftpService).localFile(tremblFastaUrl);
    verify(ftpService).downloadFile(ftpClient, tremblFastaUrl, trembl, progressBar, locale);
    verify(ftpService).localFile(idmappingUrl);
    verify(ftpService).downloadFile(ftpClient, idmappingUrl, idmapping, progressBar, locale);
    assertEquals(expectedSwissprotFasta + expectedTremblFasta,
        new String(Files.readAllBytes(output), FASTA_CHARSET));
  }

  @Test
  public void allProteinMappings_9606() throws Throwable {
    final int organismId = 9606;
    when(organism.getId()).thenReturn(organismId);
    DownloadParametersBean parameters = new DownloadParametersBean();
    parameters.organism(organism);
    parameters.proteinDatabase(ProteinDatabase.UNIPROT);
    Set<Integer> taxonChildrenIds = new HashSet<>();
    when(downloadTaxonomyService.children(any(), any(), any()))
        .thenReturn(new HashSet<>(taxonChildrenIds));
    Map<Integer, List<String>> taxonIds = taxonChildrenIds.stream()
        .collect(Collectors.toMap(t -> t, t -> generateUniprotAccessions(20)));
    taxonIds.put(organismId, generateUniprotAccessions(20));
    List<String> otherIds = generateUniprotAccessions(100);
    final String expectedSwissprotFasta = taxonIds.values().stream()
        .map(ids -> generateFastaSequences(ids.subList(0, 5))).collect(Collectors.joining());
    final String expectedTremblFasta = taxonIds.values().stream()
        .map(ids -> generateFastaSequences(ids.subList(5, 20))).collect(Collectors.joining());
    final String otherSwissprotFasta = generateFastaSequences(otherIds.subList(0, 20));
    final String otherTremblFasta = generateFastaSequences(otherIds.subList(20, 100));
    final String swissprotFasta = otherSwissprotFasta + "\n" + expectedSwissprotFasta;
    final String tremblFasta = otherTremblFasta + "\n" + expectedTremblFasta;
    Path swissprot = temporaryFolder.getRoot().toPath().resolve("pub/swissprot.fasta.gz");
    Files.createDirectories(swissprot.getParent());
    Files.write(swissprot, gzip(swissprotFasta.getBytes(FASTA_CHARSET)));
    when(ftpService.localFile(swissprotFastaUrl)).thenReturn(swissprot);
    Path trembl = temporaryFolder.getRoot().toPath().resolve("pub/trembl.fasta.gz");
    Files.createDirectories(trembl.getParent());
    Files.write(trembl, gzip(tremblFasta.getBytes(FASTA_CHARSET)));
    when(ftpService.localFile(tremblFastaUrl)).thenReturn(trembl);
    Path idmapping = temporaryFolder.getRoot().toPath().resolve("pub/idmapping.gz");
    Files.createDirectories(idmapping.getParent());
    String idmappingContent = generateIdMapping(otherIds, "4932");
    idmappingContent += taxonIds.entrySet().stream()
        .map(e -> generateIdMapping(e.getValue(), String.valueOf(e.getKey())))
        .collect(Collectors.joining());
    Files.write(idmapping, gzip(idmappingContent.getBytes(FASTA_CHARSET)));
    when(ftpService.localFile(idmappingUrl)).thenReturn(idmapping);
    final Path output = temporaryFolder.newFile("output.fasta").toPath();

    downloadServiceUniprot.downloadProteinDatabase(parameters, output, progressBar, locale);

    verify(downloadTaxonomyService).children(organismId, progressBar, locale);
    verify(ftpService, atLeastOnce()).anonymousConnect(ftp);
    verify(ftpService).localFile(swissprotFastaUrl);
    verify(ftpService).downloadFile(ftpClient, swissprotFastaUrl, swissprot, progressBar, locale);
    verify(ftpService).localFile(tremblFastaUrl);
    verify(ftpService).downloadFile(ftpClient, tremblFastaUrl, trembl, progressBar, locale);
    verify(ftpService).localFile(idmappingUrl);
    verify(ftpService).downloadFile(ftpClient, idmappingUrl, idmapping, progressBar, locale);
    assertEquals(expectedSwissprotFasta + expectedTremblFasta,
        new String(Files.readAllBytes(output), FASTA_CHARSET));
  }

  @Test
  public void allProteinMappings_ProteinDatabase_NotUniprot() throws Throwable {
    final int organismId = 9606;
    when(organism.getId()).thenReturn(organismId);
    DownloadParametersBean parameters = new DownloadParametersBean();
    parameters.organism(organism);
    parameters.proteinDatabase(ProteinDatabase.REFSEQ);
    Set<Integer> taxonChildrenIds = new HashSet<>();
    when(downloadTaxonomyService.children(any(), any(), any()))
        .thenReturn(new HashSet<>(taxonChildrenIds));
    Map<Integer, List<String>> taxonIds = taxonChildrenIds.stream()
        .collect(Collectors.toMap(t -> t, t -> generateUniprotAccessions(20)));
    taxonIds.put(organismId, generateUniprotAccessions(20));
    List<String> otherIds = generateUniprotAccessions(100);
    final String expectedSwissprotFasta = taxonIds.values().stream()
        .map(ids -> generateFastaSequences(ids.subList(0, 5))).collect(Collectors.joining());
    final String expectedTremblFasta = taxonIds.values().stream()
        .map(ids -> generateFastaSequences(ids.subList(5, 20))).collect(Collectors.joining());
    final String otherSwissprotFasta = generateFastaSequences(otherIds.subList(0, 20));
    final String otherTremblFasta = generateFastaSequences(otherIds.subList(20, 100));
    final String swissprotFasta = otherSwissprotFasta + "\n" + expectedSwissprotFasta;
    final String tremblFasta = otherTremblFasta + "\n" + expectedTremblFasta;
    Path swissprot = temporaryFolder.getRoot().toPath().resolve("swissprot.fasta.gz");
    Files.createDirectories(swissprot.getParent());
    Files.write(swissprot, gzip(swissprotFasta.getBytes(FASTA_CHARSET)));
    when(ftpService.localFile(swissprotFastaUrl)).thenReturn(swissprot);
    Path trembl = temporaryFolder.getRoot().toPath().resolve("trembl.fasta.gz");
    Files.createDirectories(trembl.getParent());
    Files.write(trembl, gzip(tremblFasta.getBytes(FASTA_CHARSET)));
    when(ftpService.localFile(tremblFastaUrl)).thenReturn(trembl);
    Path idmapping = temporaryFolder.getRoot().toPath().resolve("idmapping.gz");
    Files.createDirectories(idmapping.getParent());
    String idmappingContent = generateIdMapping(otherIds, "4932");
    idmappingContent += taxonIds.entrySet().stream()
        .map(e -> generateIdMapping(e.getValue(), String.valueOf(e.getKey())))
        .collect(Collectors.joining());
    Files.write(idmapping, gzip(idmappingContent.getBytes(FASTA_CHARSET)));
    when(ftpService.localFile(idmappingUrl)).thenReturn(idmapping);
    final Path output = temporaryFolder.newFile("output.fasta").toPath();

    try {
      downloadServiceUniprot.downloadProteinDatabase(parameters, output, progressBar, locale);
      fail("Expected IllegalArgumentException");
    } catch (IllegalArgumentException e) {
      // Success.
    }
  }

  @Test
  public void allProteinMappings_ProteinDatabase_Prohits() throws Throwable {
    final int organismId = 9606;
    when(organism.getId()).thenReturn(organismId);
    DownloadParametersBean parameters = new DownloadParametersBean();
    parameters.organism(organism);
    parameters.proteinDatabase(ProteinDatabase.UNIPROT);
    parameters.prohits(true);
    Set<Integer> taxonChildrenIds = new HashSet<>();
    when(downloadTaxonomyService.children(any(), any(), any()))
        .thenReturn(new HashSet<>(taxonChildrenIds));
    Map<Integer, List<String>> taxonIds = taxonChildrenIds.stream()
        .collect(Collectors.toMap(t -> t, t -> generateUniprotAccessions(20)));
    taxonIds.put(organismId, generateUniprotAccessions(20));
    List<String> otherIds = generateUniprotAccessions(100);
    final String expectedSwissprotFasta = taxonIds.values().stream()
        .map(ids -> generateFastaSequences(ids.subList(0, 5))).collect(Collectors.joining());
    final String expectedTremblFasta = taxonIds.values().stream()
        .map(ids -> generateFastaSequences(ids.subList(5, 20))).collect(Collectors.joining());
    final String otherSwissprotFasta = generateFastaSequences(otherIds.subList(0, 20));
    final String otherTremblFasta = generateFastaSequences(otherIds.subList(20, 100));
    final String swissprotFasta = otherSwissprotFasta + "\n" + expectedSwissprotFasta;
    final String tremblFasta = otherTremblFasta + "\n" + expectedTremblFasta;
    Path swissprot = temporaryFolder.getRoot().toPath().resolve("swissprot.fasta.gz");
    Files.createDirectories(swissprot.getParent());
    Files.write(swissprot, gzip(swissprotFasta.getBytes(FASTA_CHARSET)));
    when(ftpService.localFile(swissprotFastaUrl)).thenReturn(swissprot);
    Path trembl = temporaryFolder.getRoot().toPath().resolve("trembl.fasta.gz");
    Files.createDirectories(trembl.getParent());
    Files.write(trembl, gzip(tremblFasta.getBytes(FASTA_CHARSET)));
    when(ftpService.localFile(tremblFastaUrl)).thenReturn(trembl);
    Path idmapping = temporaryFolder.getRoot().toPath().resolve("idmapping.gz");
    Files.createDirectories(idmapping.getParent());
    String idmappingContent = generateIdMapping(otherIds, "4932");
    idmappingContent += taxonIds.entrySet().stream()
        .map(e -> generateIdMapping(e.getValue(), String.valueOf(e.getKey())))
        .collect(Collectors.joining());
    Files.write(idmapping, gzip(idmappingContent.getBytes(FASTA_CHARSET)));
    when(ftpService.localFile(idmappingUrl)).thenReturn(idmapping);
    final Path output = temporaryFolder.newFile("output.fasta").toPath();

    downloadServiceUniprot.downloadProteinDatabase(parameters, output, progressBar, locale);

    verify(downloadTaxonomyService).children(organismId, progressBar, locale);
    verify(ftpService, atLeastOnce()).anonymousConnect(ftp);
    verify(ftpService).localFile(swissprotFastaUrl);
    verify(ftpService).downloadFile(ftpClient, swissprotFastaUrl, swissprot, progressBar, locale);
    verify(ftpService).localFile(tremblFastaUrl);
    verify(ftpService).downloadFile(ftpClient, tremblFastaUrl, trembl, progressBar, locale);
    verify(ftpService).localFile(idmappingUrl);
    verify(ftpService).downloadFile(ftpClient, idmappingUrl, idmapping, progressBar, locale);
    assertEquals(
        expectedSwissprotFasta + expectedTremblFasta
            + new String(Files.readAllBytes(
                Paths.get(getClass().getResource("/prohits_additions.fasta").toURI()))),
        new String(Files.readAllBytes(output), FASTA_CHARSET));
  }

  @Test
  public void allProteinMappings_ProteinDatabase_Decoy() throws Throwable {
    final int organismId = 9606;
    when(organism.getId()).thenReturn(organismId);
    DownloadParametersBean parameters = new DownloadParametersBean();
    parameters.organism(organism);
    parameters.proteinDatabase(ProteinDatabase.UNIPROT);
    parameters.decoy(true);
    Set<Integer> taxonChildrenIds = new HashSet<>();
    when(downloadTaxonomyService.children(any(), any(), any()))
        .thenReturn(new HashSet<>(taxonChildrenIds));
    Map<Integer, List<String>> taxonIds = taxonChildrenIds.stream()
        .collect(Collectors.toMap(t -> t, t -> generateUniprotAccessions(20)));
    taxonIds.put(organismId, generateUniprotAccessions(20));
    List<String> otherIds = generateUniprotAccessions(100);
    final String expectedSwissprotFasta = taxonIds.values().stream()
        .map(ids -> generateFastaSequences(ids.subList(0, 5))).collect(Collectors.joining());
    final String expectedTremblFasta = taxonIds.values().stream()
        .map(ids -> generateFastaSequences(ids.subList(5, 20))).collect(Collectors.joining());
    final String otherSwissprotFasta = generateFastaSequences(otherIds.subList(0, 20));
    final String otherTremblFasta = generateFastaSequences(otherIds.subList(20, 100));
    final String swissprotFasta = otherSwissprotFasta + "\n" + expectedSwissprotFasta;
    final String tremblFasta = otherTremblFasta + "\n" + expectedTremblFasta;
    Path swissprot = temporaryFolder.getRoot().toPath().resolve("swissprot.fasta.gz");
    Files.createDirectories(swissprot.getParent());
    Files.write(swissprot, gzip(swissprotFasta.getBytes(FASTA_CHARSET)));
    when(ftpService.localFile(swissprotFastaUrl)).thenReturn(swissprot);
    Path trembl = temporaryFolder.getRoot().toPath().resolve("trembl.fasta.gz");
    Files.createDirectories(trembl.getParent());
    Files.write(trembl, gzip(tremblFasta.getBytes(FASTA_CHARSET)));
    when(ftpService.localFile(tremblFastaUrl)).thenReturn(trembl);
    Path idmapping = temporaryFolder.getRoot().toPath().resolve("idmapping.gz");
    Files.createDirectories(idmapping.getParent());
    String idmappingContent = generateIdMapping(otherIds, "4932");
    idmappingContent += taxonIds.entrySet().stream()
        .map(e -> generateIdMapping(e.getValue(), String.valueOf(e.getKey())))
        .collect(Collectors.joining());
    Files.write(idmapping, gzip(idmappingContent.getBytes(FASTA_CHARSET)));
    when(ftpService.localFile(idmappingUrl)).thenReturn(idmapping);
    final Path output = temporaryFolder.newFile("output.fasta").toPath();

    downloadServiceUniprot.downloadProteinDatabase(parameters, output, progressBar, locale);

    verify(downloadTaxonomyService).children(organismId, progressBar, locale);
    verify(ftpService, atLeastOnce()).anonymousConnect(ftp);
    verify(ftpService).localFile(swissprotFastaUrl);
    verify(ftpService).downloadFile(ftpClient, swissprotFastaUrl, swissprot, progressBar, locale);
    verify(ftpService).localFile(tremblFastaUrl);
    verify(ftpService).downloadFile(ftpClient, tremblFastaUrl, trembl, progressBar, locale);
    verify(ftpService).localFile(idmappingUrl);
    verify(ftpService).downloadFile(ftpClient, idmappingUrl, idmapping, progressBar, locale);
    for (int aa = 'A'; aa <= 'Z'; aa++) {
      int[] counts = count(aa, output);
      int max = Math.max(counts[0], counts[1]);
      int min = Math.min(counts[0], counts[1]);
      double diff;
      if (max == 0) {
        diff = 1.0;
      } else {
        diff = (double) min / max;
      }
      assertEquals(1.0, diff, 0.4);
    }
  }

  @Test
  public void allProteinMappings_4930_Swissprot() throws Throwable {
    final int organismId = 4930;
    when(organism.getId()).thenReturn(organismId);
    DownloadParametersBean parameters = new DownloadParametersBean();
    parameters.organism(organism);
    parameters.proteinDatabase(ProteinDatabase.SWISSPROT);
    List<Integer> taxonChildrenIds = new ArrayList<>();
    taxonChildrenIds.add(4932);
    taxonChildrenIds.add(765312);
    taxonChildrenIds.add(545124);
    taxonChildrenIds.add(1337652);
    when(downloadTaxonomyService.children(any(), any(), any()))
        .thenReturn(new HashSet<>(taxonChildrenIds));
    Map<Integer, List<String>> taxonIds = taxonChildrenIds.stream()
        .collect(Collectors.toMap(t -> t, t -> generateUniprotAccessions(20)));
    taxonIds.put(organismId, generateUniprotAccessions(20));
    List<String> otherIds = generateUniprotAccessions(100);
    final String expectedSwissprotFasta = taxonIds.values().stream()
        .map(ids -> generateFastaSequences(ids.subList(0, 5))).collect(Collectors.joining());
    final String expectedTremblFasta = taxonIds.values().stream()
        .map(ids -> generateFastaSequences(ids.subList(5, 20))).collect(Collectors.joining());
    final String otherSwissprotFasta = generateFastaSequences(otherIds.subList(0, 20));
    final String otherTremblFasta = generateFastaSequences(otherIds.subList(20, 100));
    final String swissprotFasta = otherSwissprotFasta + "\n" + expectedSwissprotFasta;
    final String tremblFasta = otherTremblFasta + "\n" + expectedTremblFasta;
    Path swissprot = temporaryFolder.getRoot().toPath().resolve("pub/swissprot.fasta.gz");
    Files.createDirectories(swissprot.getParent());
    Files.write(swissprot, gzip(swissprotFasta.getBytes(FASTA_CHARSET)));
    when(ftpService.localFile(swissprotFastaUrl)).thenReturn(swissprot);
    Path trembl = temporaryFolder.getRoot().toPath().resolve("pub/trembl.fasta.gz");
    Files.createDirectories(trembl.getParent());
    Files.write(trembl, gzip(tremblFasta.getBytes(FASTA_CHARSET)));
    when(ftpService.localFile(tremblFastaUrl)).thenReturn(trembl);
    Path idmapping = temporaryFolder.getRoot().toPath().resolve("pub/idmapping.gz");
    Files.createDirectories(idmapping.getParent());
    String idmappingContent = generateIdMapping(otherIds, "9606");
    idmappingContent += taxonIds.entrySet().stream()
        .map(e -> generateIdMapping(e.getValue(), String.valueOf(e.getKey())))
        .collect(Collectors.joining());
    Files.write(idmapping, gzip(idmappingContent.getBytes(FASTA_CHARSET)));
    when(ftpService.localFile(idmappingUrl)).thenReturn(idmapping);
    final Path output = temporaryFolder.newFile("output.fasta").toPath();

    downloadServiceUniprot.downloadProteinDatabase(parameters, output, progressBar, locale);

    verify(downloadTaxonomyService).children(organismId, progressBar, locale);
    verify(ftpService, atLeastOnce()).anonymousConnect(ftp);
    verify(ftpService).localFile(swissprotFastaUrl);
    verify(ftpService).downloadFile(ftpClient, swissprotFastaUrl, swissprot, progressBar, locale);
    verify(ftpService).localFile(idmappingUrl);
    verify(ftpService).downloadFile(ftpClient, idmappingUrl, idmapping, progressBar, locale);
    assertEquals(expectedSwissprotFasta, new String(Files.readAllBytes(output), FASTA_CHARSET));
  }

  @Test
  public void allProteinMappings_9606_Swissprot() throws Throwable {
    final int organismId = 9606;
    when(organism.getId()).thenReturn(organismId);
    DownloadParametersBean parameters = new DownloadParametersBean();
    parameters.organism(organism);
    parameters.proteinDatabase(ProteinDatabase.SWISSPROT);
    Set<Integer> taxonChildrenIds = new HashSet<>();
    when(downloadTaxonomyService.children(any(), any(), any()))
        .thenReturn(new HashSet<>(taxonChildrenIds));
    Map<Integer, List<String>> taxonIds = taxonChildrenIds.stream()
        .collect(Collectors.toMap(t -> t, t -> generateUniprotAccessions(20)));
    taxonIds.put(organismId, generateUniprotAccessions(20));
    List<String> otherIds = generateUniprotAccessions(100);
    final String expectedSwissprotFasta = taxonIds.values().stream()
        .map(ids -> generateFastaSequences(ids.subList(0, 5))).collect(Collectors.joining());
    final String expectedTremblFasta = taxonIds.values().stream()
        .map(ids -> generateFastaSequences(ids.subList(5, 20))).collect(Collectors.joining());
    final String otherSwissprotFasta = generateFastaSequences(otherIds.subList(0, 20));
    final String otherTremblFasta = generateFastaSequences(otherIds.subList(20, 100));
    final String swissprotFasta = otherSwissprotFasta + "\n" + expectedSwissprotFasta;
    final String tremblFasta = otherTremblFasta + "\n" + expectedTremblFasta;
    Path swissprot = temporaryFolder.getRoot().toPath().resolve("pub/swissprot.fasta.gz");
    Files.createDirectories(swissprot.getParent());
    Files.write(swissprot, gzip(swissprotFasta.getBytes(FASTA_CHARSET)));
    when(ftpService.localFile(swissprotFastaUrl)).thenReturn(swissprot);
    Path trembl = temporaryFolder.getRoot().toPath().resolve("pub/trembl.fasta.gz");
    Files.createDirectories(trembl.getParent());
    Files.write(trembl, gzip(tremblFasta.getBytes(FASTA_CHARSET)));
    when(ftpService.localFile(tremblFastaUrl)).thenReturn(trembl);
    Path idmapping = temporaryFolder.getRoot().toPath().resolve("pub/idmapping.gz");
    Files.createDirectories(idmapping.getParent());
    String idmappingContent = generateIdMapping(otherIds, "4932");
    idmappingContent += taxonIds.entrySet().stream()
        .map(e -> generateIdMapping(e.getValue(), String.valueOf(e.getKey())))
        .collect(Collectors.joining());
    Files.write(idmapping, gzip(idmappingContent.getBytes(FASTA_CHARSET)));
    when(ftpService.localFile(idmappingUrl)).thenReturn(idmapping);
    final Path output = temporaryFolder.newFile("output.fasta").toPath();

    downloadServiceUniprot.downloadProteinDatabase(parameters, output, progressBar, locale);

    verify(downloadTaxonomyService).children(organismId, progressBar, locale);
    verify(ftpService, atLeastOnce()).anonymousConnect(ftp);
    verify(ftpService).localFile(swissprotFastaUrl);
    verify(ftpService).downloadFile(ftpClient, swissprotFastaUrl, swissprot, progressBar, locale);
    verify(ftpService).localFile(idmappingUrl);
    verify(ftpService).downloadFile(ftpClient, idmappingUrl, idmapping, progressBar, locale);
    assertEquals(expectedSwissprotFasta, new String(Files.readAllBytes(output), FASTA_CHARSET));
  }

  private int[] count(int aa, Path fasta) throws IOException {
    int[] counts = new int[2];
    int index = -1;
    for (String line : Files.readAllLines(fasta)) {
      if (line.startsWith(";")) {
        continue;
      } else if (line.startsWith(">DECOY")) {
        index = 1;
      } else if (line.startsWith(">")) {
        index = 0;
      } else if (index >= 0) {
        counts[index] += line.chars().filter(l -> l == aa).count();
      }
    }
    return counts;
  }
}
