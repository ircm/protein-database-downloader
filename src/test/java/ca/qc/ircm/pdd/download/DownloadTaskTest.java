/*
 * Copyright (c) 2006 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.pdd.download;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;

import ca.qc.ircm.pdd.organism.Organism;
import ca.qc.ircm.pdd.test.config.RetryOnFail;
import ca.qc.ircm.pdd.test.config.TestFxTestAnnotations;
import ca.qc.ircm.progressbar.ProgressBar;
import java.nio.file.Path;
import java.util.Locale;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.testfx.framework.junit.ApplicationTest;

@RunWith(SpringJUnit4ClassRunner.class)
@TestFxTestAnnotations
public class DownloadTaskTest extends ApplicationTest {
  private DownloadProteinDatabaseTask downloadProteinDatabaseTask;
  @Mock
  private Organism organism;
  @Mock
  private Path file;
  @Mock
  private DownloadService downloadService;
  @Mock
  private ChangeListener<String> messageChangeListener;
  @Mock
  private ChangeListener<Number> progressChangeListener;
  @Mock
  private EventHandler<WorkerStateEvent> cancelHandler;
  @Captor
  private ArgumentCaptor<ObservableValue<String>> observableMessageCaptor;
  @Captor
  private ArgumentCaptor<ObservableValue<Number>> observableProgressCaptor;
  private ProteinDatabase database;
  private DownloadParametersBean parameters;
  private Locale locale;

  @Override
  public void start(Stage stage) throws Exception {
  }

  /**
   * Before test.
   */
  @Before
  public void beforeTest() throws Throwable {
    locale = Locale.getDefault();
    database = ProteinDatabase.NCBI;
    parameters = new DownloadParametersBean();
    parameters.organism(organism);
    parameters.proteinDatabase(database);
    downloadProteinDatabaseTask =
        new DownloadProteinDatabaseTask(parameters, file, locale, downloadService);
  }

  @Test
  public void call() throws Throwable {
    downloadProteinDatabaseTask.messageProperty().addListener(messageChangeListener);
    downloadProteinDatabaseTask.progressProperty().addListener(progressChangeListener);
    doAnswer(new Answer<Void>() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        ProgressBar progressBar = (ProgressBar) invocation.getArguments()[2];
        if (progressBar != null) {
          progressBar.setMessage("fillGeneDatabase");
          progressBar.setProgress(1.0);
        }
        return null;
      }
    }).when(downloadService).downloadProteinDatabase(any(), any(), any(), any(Locale.class));

    downloadProteinDatabaseTask.call();

    verify(downloadService).downloadProteinDatabase(eq(parameters), eq(file),
        any(ProgressBar.class), eq(locale));
    verify(messageChangeListener, atLeastOnce()).changed(observableMessageCaptor.capture(),
        any(String.class), any(String.class));
    verify(progressChangeListener, atLeastOnce()).changed(observableProgressCaptor.capture(),
        any(Number.class), any(Number.class));
  }

  @Test
  @RetryOnFail(5)
  public void cancel() throws Throwable {
    downloadProteinDatabaseTask.setOnCancelled(cancelHandler);

    new Thread(() -> downloadProteinDatabaseTask.cancel()).start();

    Thread.sleep(1000);
    verify(cancelHandler).handle(any(WorkerStateEvent.class));
  }
}
