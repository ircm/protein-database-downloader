/*
 * Copyright (c) 2006 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.pdd.download;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import ca.qc.ircm.pdd.annotation.NcbiConfiguration;
import ca.qc.ircm.pdd.annotation.SequenceConfiguration;
import ca.qc.ircm.pdd.ftp.FtpService;
import ca.qc.ircm.pdd.organism.Organism;
import ca.qc.ircm.pdd.rest.RestClientFactory;
import ca.qc.ircm.pdd.test.config.RetryOnFail;
import ca.qc.ircm.pdd.test.config.RetryOnFailRule;
import ca.qc.ircm.pdd.test.config.ServiceTestAnnotations;
import ca.qc.ircm.progressbar.ProgressBar;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.glassfish.jersey.client.ClientProperties;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ServiceTestAnnotations
public class DownloadServiceEutilsTest {
  private static final int SEARCH_COUNT = 1231;
  private static final int MAX_IDS_PER_REQUEST = 500;
  private static final int FETCH_COUNT =
      (int) Math.ceil((double) SEARCH_COUNT / MAX_IDS_PER_REQUEST);
  private static final Charset FASTA_CHARSET = Charset.forName("UTF-8");

  private DownloadServiceEutils downloadServiceEutils;
  @Mock
  private SequenceConfiguration sequenceConfiguration;
  @Mock
  private NcbiConfiguration ncbiConfiguration;
  @Mock
  private Organism organism;
  @Mock
  private ProgressBar progressBar;
  @Mock
  private RestClientFactory restClientFactory;
  @Mock
  private FtpService ftpService;
  @Mock
  private Client clientSearch;
  @Mock
  private Client clientFetchIds;
  @Mock
  private Client clientFetchFasta;
  @Mock
  private WebTarget targetSearch;
  @Mock
  private WebTarget targetFetchIds;
  @Mock
  private WebTarget targetFetchFasta;
  @Mock
  private Invocation.Builder invocationSearch;
  @Mock
  private Invocation.Builder invocationFetchIds;
  @Mock
  private Invocation.Builder invocationFetchFasta;
  @Mock
  private FTPClient ftpClient;
  @Captor
  private ArgumentCaptor<Entity<?>> entityCaptor;
  @Rule
  public RetryOnFailRule retryOnFailRule = new RetryOnFailRule();
  @Rule
  public TemporaryFolder temporaryFolder = new TemporaryFolder();
  private Locale locale;
  private String eutilsUrl;
  private String ftpUrl;
  private String releaseNumberUrl;
  private List<String> proteinIds = new ArrayList<>();

  /**
   * Before test.
   */
  @Before
  public void beforeTest() throws Throwable {
    downloadServiceEutils = new DownloadServiceEutils(sequenceConfiguration, ncbiConfiguration,
        restClientFactory, ftpService);
    locale = Locale.getDefault();
    eutilsUrl = "localhost:8080/eutils";
    ftpUrl = "localhost:8080";
    releaseNumberUrl = "/releaseNumber";
    when(ncbiConfiguration.getEutils()).thenReturn(eutilsUrl);
    when(ncbiConfiguration.getMaxIdsPerRequest()).thenReturn(MAX_IDS_PER_REQUEST);
    when(ncbiConfiguration.getFtp()).thenReturn(ftpUrl);
    when(ncbiConfiguration.getRefseqReleaseNumber()).thenReturn(releaseNumberUrl);
    when(sequenceConfiguration.getDecoyName()).thenReturn("DECOY{0,number,#}");
    when(clientSearch.target(any(String.class))).thenReturn(targetSearch);
    when(clientFetchIds.target(any(String.class))).thenReturn(targetFetchIds);
    when(clientFetchFasta.target(any(String.class))).thenReturn(targetFetchFasta);
    when(targetSearch.path(any())).thenReturn(targetSearch);
    when(targetSearch.queryParam(any(), any())).thenReturn(targetSearch);
    when(targetSearch.request()).thenReturn(invocationSearch);
    when(targetFetchIds.path(any())).thenReturn(targetFetchIds);
    when(targetFetchIds.queryParam(any(), any())).thenReturn(targetFetchIds);
    when(targetFetchIds.request()).thenReturn(invocationFetchIds);
    when(ftpService.anonymousConnect(any())).thenReturn(ftpClient);
    when(targetFetchFasta.path(any())).thenReturn(targetFetchFasta);
    when(targetFetchFasta.queryParam(any(), any())).thenReturn(targetFetchFasta);
    when(targetFetchFasta.request()).thenReturn(invocationFetchFasta);
    when(progressBar.step(anyDouble())).thenReturn(progressBar);
    doAnswer(i -> {
      System.out.println(i.getArguments()[0]);
      return null;
    }).when(progressBar).setMessage(any());
    parseProteinIds(Paths.get(getClass().getResource("/gis.txt").toURI()));
  }

  private void parseProteinIds(Path path) throws IOException {
    proteinIds.clear();
    Files.readAllLines(path).forEach(line -> proteinIds.add(line));
  }

  private String generateFastaSequences(List<String> ids) {
    StringBuilder sequences = new StringBuilder();
    ids.forEach(id -> {
      sequences.append(">db|" + id + " " + RandomStringUtils.randomAlphanumeric(40));
      sequences.append("\n");
      sequences.append(RandomStringUtils.randomAlphabetic(80).toUpperCase());
      sequences.append("\n");
      sequences.append(RandomStringUtils.randomAlphabetic(40).toUpperCase());
      sequences.append("\n");
      sequences.append("\n");
    });
    return sequences.toString();
  }

  private String parseContent(Path file) throws IOException {
    return new String(Files.readAllBytes(file), FASTA_CHARSET);
  }

  private void validateContent(List<String> fastas, Path actual) throws IOException {
    ByteArrayOutputStream actualOutput = new ByteArrayOutputStream();
    try (InputStream actualInput = Files.newInputStream(actual)) {
      IOUtils.copyLarge(actualInput, actualOutput);
    }
    String actualContent = new String(actualOutput.toByteArray(), FASTA_CHARSET);
    StringBuilder expectedContentBuilder = new StringBuilder();
    fastas.forEach(fasta -> {
      expectedContentBuilder.append(fasta);
    });
    String expectedContent = expectedContentBuilder.toString();
    assertArrayEquals(expectedContent.split("\r?\n", -1), actualContent.split("\r?\n", -1));
  }

  @Test
  public void allProteinMappings_9606() throws Throwable {
    Path esearch = Paths.get(getClass().getResource("/esearch.fcgi.xml").toURI());
    List<String> fastas = new ArrayList<>();
    IntStream.range(0, FETCH_COUNT).forEach(i -> {
      int start = i * MAX_IDS_PER_REQUEST;
      fastas.add(generateFastaSequences(
          proteinIds.subList(start, Math.min(start + MAX_IDS_PER_REQUEST, proteinIds.size()))));
    });
    when(organism.getId()).thenReturn(9606);
    final Path output = temporaryFolder.newFile("output.fasta").toPath();
    when(restClientFactory.createClient()).thenReturn(clientSearch, clientFetchIds,
        clientFetchFasta);
    when(invocationSearch.get(InputStream.class)).thenAnswer(i -> Files.newInputStream(esearch));
    InputStream firstIdsReturn = new ByteArrayInputStream(proteinIds.stream()
        .limit(MAX_IDS_PER_REQUEST).collect(Collectors.joining("\n")).getBytes(FASTA_CHARSET));
    InputStream[] idsReturns = new InputStream[FETCH_COUNT - 1];
    IntStream.range(1, FETCH_COUNT).forEach(i -> {
      try {
        String accesssionsAsString = proteinIds.stream().skip(i * MAX_IDS_PER_REQUEST)
            .limit(MAX_IDS_PER_REQUEST).collect(Collectors.joining("\n"));
        idsReturns[i - 1] = new ByteArrayInputStream(accesssionsAsString.getBytes(FASTA_CHARSET));
      } catch (Exception e) {
        throw new IllegalStateException();
      }
    });
    when(invocationFetchIds.get(eq(InputStream.class))).thenReturn(firstIdsReturn, idsReturns);
    Path releaseNumber = Paths.get(getClass().getResource("/releaseNumber.txt").toURI());
    when(ftpService.localFile(releaseNumberUrl)).thenReturn(releaseNumber);
    InputStream[] fastaReturns = new InputStream[FETCH_COUNT - 1];
    IntStream.range(1, FETCH_COUNT).forEach(i -> {
      try {
        fastaReturns[i - 1] = new ByteArrayInputStream(fastas.get(i).getBytes(FASTA_CHARSET));
      } catch (Exception e) {
        throw new IllegalStateException();
      }
    });
    when(invocationFetchFasta.post(any(), eq(InputStream.class)))
        .thenReturn(new ByteArrayInputStream(fastas.get(0).getBytes(FASTA_CHARSET)), fastaReturns);
    DownloadParametersBean parameters = new DownloadParametersBean();
    parameters.organism(organism);
    parameters.proteinDatabase(ProteinDatabase.REFSEQ);

    final String message =
        downloadServiceEutils.downloadProteinDatabase(parameters, output, progressBar, locale);

    verify(restClientFactory, times(FETCH_COUNT + 2)).createClient();
    verify(clientSearch).property(ClientProperties.CONNECT_TIMEOUT,
        DownloadServiceAdditionalSequences.REST_TIMEOUT);
    verify(clientSearch).target(eutilsUrl);
    verify(targetSearch).path("esearch.fcgi");
    verify(targetSearch).queryParam("db", "protein");
    verify(targetSearch).queryParam("term", "txid9606[Organism] AND refseq[filter]");
    verify(targetSearch).queryParam("usehistory", "y");
    verify(targetSearch).request();
    verify(invocationSearch).get(InputStream.class);
    verify(clientFetchIds).property(ClientProperties.CONNECT_TIMEOUT,
        DownloadServiceAdditionalSequences.REST_TIMEOUT);
    verify(clientFetchIds).target(eutilsUrl);
    verify(ftpService).anonymousConnect(ftpUrl);
    verify(ftpService).localFile(releaseNumberUrl);
    verify(ftpService).downloadFile(ftpClient, releaseNumberUrl, releaseNumber, progressBar,
        locale);
    verify(targetFetchIds).path("efetch.fcgi");
    verify(targetFetchIds).queryParam("db", "protein");
    verify(targetFetchIds).queryParam("WebEnv",
        "NCID_1_174988986_130.14.22.215_9001_1434470567_1270571334_0MetA0_S_MegaStore_F_1");
    verify(targetFetchIds).queryParam("query_key", "1");
    verify(targetFetchIds).queryParam("rettype", "acc");
    verify(targetFetchIds).queryParam("retmax", MAX_IDS_PER_REQUEST);
    IntStream.range(0, FETCH_COUNT).forEach(i -> {
      verify(targetFetchIds).queryParam("retstart", i * MAX_IDS_PER_REQUEST);
    });
    verify(targetFetchIds, times(FETCH_COUNT)).request();
    verify(invocationFetchIds, times(FETCH_COUNT)).get(InputStream.class);
    verify(clientFetchFasta, times(FETCH_COUNT)).property(ClientProperties.CONNECT_TIMEOUT,
        DownloadServiceAdditionalSequences.REST_TIMEOUT);
    verify(clientFetchFasta, times(FETCH_COUNT)).target(eutilsUrl);
    verify(targetFetchFasta, times(FETCH_COUNT)).path("efetch.fcgi");
    verify(targetFetchFasta, times(FETCH_COUNT)).request();
    verify(invocationFetchFasta, times(FETCH_COUNT)).post(entityCaptor.capture(),
        eq(InputStream.class));
    IntStream.range(0, FETCH_COUNT).forEach(i -> {
      Entity<?> entity = entityCaptor.getAllValues().get(i);
      assertTrue(entity.getEntity() instanceof Form);
      assertEquals(MediaType.APPLICATION_FORM_URLENCODED_TYPE, entity.getMediaType());
      MultivaluedMap<String, String> formParams = ((Form) entity.getEntity()).asMap();
      assertEquals(1, formParams.get("db").size());
      assertEquals("protein", formParams.get("db").get(0));
      int start = i * MAX_IDS_PER_REQUEST;
      List<String> expectedIds = new ArrayList<>(
          proteinIds.subList(start, Math.min(start + MAX_IDS_PER_REQUEST, proteinIds.size())));
      List<String> actualIds = formParams.get("id");
      Collections.sort(expectedIds);
      Collections.sort(actualIds);
      assertEquals(expectedIds, actualIds);
      assertEquals(1, formParams.get("rettype").size());
      assertEquals("fasta", formParams.get("rettype").get(0));
    });
    validateContent(fastas, output);
    assertTrue(message.contains(String.valueOf(fastas.size())));
    assertTrue(message.contains("68"));
  }

  @Test
  public void allProteinMappings_9606_Ncbi() throws Throwable {
    Path esearch = Paths.get(getClass().getResource("/esearch.fcgi.xml").toURI());
    List<String> fastas = new ArrayList<>();
    IntStream.range(0, FETCH_COUNT).forEach(i -> {
      int start = i * MAX_IDS_PER_REQUEST;
      fastas.add(generateFastaSequences(
          proteinIds.subList(start, Math.min(start + MAX_IDS_PER_REQUEST, proteinIds.size()))));
    });
    when(organism.getId()).thenReturn(9606);
    final Path output = temporaryFolder.newFile("output.fasta").toPath();
    when(restClientFactory.createClient()).thenReturn(clientSearch, clientFetchIds,
        clientFetchFasta);
    when(invocationSearch.get(InputStream.class)).thenAnswer(i -> Files.newInputStream(esearch));
    InputStream firstIdsReturn = new ByteArrayInputStream(proteinIds.stream()
        .limit(MAX_IDS_PER_REQUEST).collect(Collectors.joining("\n")).getBytes(FASTA_CHARSET));
    InputStream[] idsReturns = new InputStream[FETCH_COUNT - 1];
    IntStream.range(1, FETCH_COUNT).forEach(i -> {
      try {
        String accesssionsAsString = proteinIds.stream().skip(i * MAX_IDS_PER_REQUEST)
            .limit(MAX_IDS_PER_REQUEST).collect(Collectors.joining("\n"));
        idsReturns[i - 1] = new ByteArrayInputStream(accesssionsAsString.getBytes(FASTA_CHARSET));
      } catch (Exception e) {
        throw new IllegalStateException();
      }
    });
    when(invocationFetchIds.get(eq(InputStream.class))).thenReturn(firstIdsReturn, idsReturns);
    Path releaseNumber = Paths.get(getClass().getResource("/releaseNumber.txt").toURI());
    when(ftpService.localFile(releaseNumberUrl)).thenReturn(releaseNumber);
    InputStream[] fastaReturns = new InputStream[FETCH_COUNT - 1];
    IntStream.range(1, FETCH_COUNT).forEach(i -> {
      try {
        fastaReturns[i - 1] = new ByteArrayInputStream(fastas.get(i).getBytes(FASTA_CHARSET));
      } catch (Exception e) {
        throw new IllegalStateException();
      }
    });
    when(invocationFetchFasta.post(any(), eq(InputStream.class)))
        .thenReturn(new ByteArrayInputStream(fastas.get(0).getBytes(FASTA_CHARSET)), fastaReturns);
    DownloadParametersBean parameters = new DownloadParametersBean();
    parameters.organism(organism);
    parameters.proteinDatabase(ProteinDatabase.NCBI);

    final String message =
        downloadServiceEutils.downloadProteinDatabase(parameters, output, progressBar, locale);

    verify(restClientFactory, times(FETCH_COUNT + 2)).createClient();
    verify(clientSearch).property(ClientProperties.CONNECT_TIMEOUT,
        DownloadServiceAdditionalSequences.REST_TIMEOUT);
    verify(clientSearch).target(eutilsUrl);
    verify(targetSearch).path("esearch.fcgi");
    verify(targetSearch).queryParam("db", "protein");
    verify(targetSearch).queryParam("term", "txid9606[Organism]");
    verify(targetSearch).queryParam("usehistory", "y");
    verify(targetSearch).request();
    verify(invocationSearch).get(InputStream.class);
    verify(clientFetchIds).property(ClientProperties.CONNECT_TIMEOUT,
        DownloadServiceAdditionalSequences.REST_TIMEOUT);
    verify(clientFetchIds).target(eutilsUrl);
    verify(ftpService, never()).anonymousConnect(ftpUrl);
    verify(ftpService, never()).localFile(releaseNumberUrl);
    verify(ftpService, never()).downloadFile(ftpClient, releaseNumberUrl, releaseNumber,
        progressBar, locale);
    verify(targetFetchIds).path("efetch.fcgi");
    verify(targetFetchIds).queryParam("db", "protein");
    verify(targetFetchIds).queryParam("WebEnv",
        "NCID_1_174988986_130.14.22.215_9001_1434470567_1270571334_0MetA0_S_MegaStore_F_1");
    verify(targetFetchIds).queryParam("query_key", "1");
    verify(targetFetchIds).queryParam("rettype", "acc");
    verify(targetFetchIds).queryParam("retmax", MAX_IDS_PER_REQUEST);
    IntStream.range(0, FETCH_COUNT).forEach(i -> {
      verify(targetFetchIds).queryParam("retstart", i * MAX_IDS_PER_REQUEST);
    });
    verify(targetFetchIds, times(FETCH_COUNT)).request();
    verify(invocationFetchIds, times(FETCH_COUNT)).get(InputStream.class);
    verify(clientFetchFasta, times(FETCH_COUNT)).property(ClientProperties.CONNECT_TIMEOUT,
        DownloadServiceAdditionalSequences.REST_TIMEOUT);
    verify(clientFetchFasta, times(FETCH_COUNT)).target(eutilsUrl);
    verify(targetFetchFasta, times(FETCH_COUNT)).path("efetch.fcgi");
    verify(targetFetchFasta, times(FETCH_COUNT)).request();
    verify(invocationFetchFasta, times(FETCH_COUNT)).post(entityCaptor.capture(),
        eq(InputStream.class));
    IntStream.range(0, FETCH_COUNT).forEach(i -> {
      Entity<?> entity = entityCaptor.getAllValues().get(i);
      assertTrue(entity.getEntity() instanceof Form);
      assertEquals(MediaType.APPLICATION_FORM_URLENCODED_TYPE, entity.getMediaType());
      MultivaluedMap<String, String> formParams = ((Form) entity.getEntity()).asMap();
      assertEquals(1, formParams.get("db").size());
      assertEquals("protein", formParams.get("db").get(0));
      int start = i * MAX_IDS_PER_REQUEST;
      List<String> expectedIds = new ArrayList<>(
          proteinIds.subList(start, Math.min(start + MAX_IDS_PER_REQUEST, proteinIds.size())));
      List<String> actualIds = formParams.get("id");
      Collections.sort(expectedIds);
      Collections.sort(actualIds);
      assertEquals(expectedIds, actualIds);
      assertEquals(1, formParams.get("rettype").size());
      assertEquals("fasta", formParams.get("rettype").get(0));
    });
    validateContent(fastas, output);
    assertTrue(message.contains(String.valueOf(fastas.size())));
  }

  @Test
  public void allProteinMappings_4930() throws Throwable {
    Path esearch = Paths.get(getClass().getResource("/esearch.fcgi.xml").toURI());
    List<String> fastas = new ArrayList<>();
    IntStream.range(0, FETCH_COUNT).forEach(i -> {
      int start = i * MAX_IDS_PER_REQUEST;
      fastas.add(generateFastaSequences(
          proteinIds.subList(start, Math.min(start + MAX_IDS_PER_REQUEST, proteinIds.size()))));
    });
    when(organism.getId()).thenReturn(4930);
    final Path output = temporaryFolder.newFile("output.fasta").toPath();
    when(restClientFactory.createClient()).thenReturn(clientSearch, clientFetchIds,
        clientFetchFasta);
    when(invocationSearch.get(InputStream.class)).thenAnswer(i -> Files.newInputStream(esearch));
    InputStream firstIdsReturn = new ByteArrayInputStream(proteinIds.stream()
        .limit(MAX_IDS_PER_REQUEST).collect(Collectors.joining("\n")).getBytes(FASTA_CHARSET));
    InputStream[] idsReturns = new InputStream[FETCH_COUNT - 1];
    IntStream.range(1, FETCH_COUNT).forEach(i -> {
      try {
        String accesssionsAsString = proteinIds.stream().skip(i * MAX_IDS_PER_REQUEST)
            .limit(MAX_IDS_PER_REQUEST).collect(Collectors.joining("\n"));
        idsReturns[i - 1] = new ByteArrayInputStream(accesssionsAsString.getBytes(FASTA_CHARSET));
      } catch (Exception e) {
        throw new IllegalStateException();
      }
    });
    when(invocationFetchIds.get(eq(InputStream.class))).thenReturn(firstIdsReturn, idsReturns);
    Path releaseNumber = Paths.get(getClass().getResource("/releaseNumber.txt").toURI());
    when(ftpService.localFile(releaseNumberUrl)).thenReturn(releaseNumber);
    InputStream[] fastaReturns = new InputStream[FETCH_COUNT - 1];
    IntStream.range(1, FETCH_COUNT).forEach(i -> {
      try {
        fastaReturns[i - 1] = new ByteArrayInputStream(fastas.get(i).getBytes(FASTA_CHARSET));
      } catch (Exception e) {
        throw new IllegalStateException();
      }
    });
    when(invocationFetchFasta.post(any(), eq(InputStream.class)))
        .thenReturn(new ByteArrayInputStream(fastas.get(0).getBytes(FASTA_CHARSET)), fastaReturns);
    DownloadParametersBean parameters = new DownloadParametersBean();
    parameters.organism(organism);
    parameters.proteinDatabase(ProteinDatabase.REFSEQ);

    final String message =
        downloadServiceEutils.downloadProteinDatabase(parameters, output, progressBar, locale);

    verify(restClientFactory, times(FETCH_COUNT + 2)).createClient();
    verify(clientSearch).property(ClientProperties.CONNECT_TIMEOUT,
        DownloadServiceAdditionalSequences.REST_TIMEOUT);
    verify(clientSearch).target(eutilsUrl);
    verify(targetSearch).path("esearch.fcgi");
    verify(targetSearch).queryParam("db", "protein");
    verify(targetSearch).queryParam("term", "txid4930[Organism] AND refseq[filter]");
    verify(targetSearch).queryParam("usehistory", "y");
    verify(targetSearch).request();
    verify(invocationSearch).get(InputStream.class);
    verify(clientFetchIds).property(ClientProperties.CONNECT_TIMEOUT,
        DownloadServiceAdditionalSequences.REST_TIMEOUT);
    verify(clientFetchIds).target(eutilsUrl);
    verify(ftpService).anonymousConnect(ftpUrl);
    verify(ftpService).localFile(releaseNumberUrl);
    verify(ftpService).downloadFile(ftpClient, releaseNumberUrl, releaseNumber, progressBar,
        locale);
    verify(targetFetchIds).path("efetch.fcgi");
    verify(targetFetchIds).queryParam("db", "protein");
    verify(targetFetchIds).queryParam("WebEnv",
        "NCID_1_174988986_130.14.22.215_9001_1434470567_1270571334_0MetA0_S_MegaStore_F_1");
    verify(targetFetchIds).queryParam("query_key", "1");
    verify(targetFetchIds).queryParam("rettype", "acc");
    verify(targetFetchIds).queryParam("retmax", MAX_IDS_PER_REQUEST);
    IntStream.range(0, FETCH_COUNT).forEach(i -> {
      verify(targetFetchIds).queryParam("retstart", i * MAX_IDS_PER_REQUEST);
    });
    verify(targetFetchIds, times(FETCH_COUNT)).request();
    verify(invocationFetchIds, times(FETCH_COUNT)).get(InputStream.class);
    verify(clientFetchFasta, times(FETCH_COUNT)).property(ClientProperties.CONNECT_TIMEOUT,
        DownloadServiceAdditionalSequences.REST_TIMEOUT);
    verify(clientFetchFasta, times(FETCH_COUNT)).target(eutilsUrl);
    verify(targetFetchFasta, times(FETCH_COUNT)).path("efetch.fcgi");
    verify(targetFetchFasta, times(FETCH_COUNT)).request();
    verify(invocationFetchFasta, times(FETCH_COUNT)).post(entityCaptor.capture(),
        eq(InputStream.class));
    IntStream.range(0, FETCH_COUNT).forEach(i -> {
      Entity<?> entity = entityCaptor.getAllValues().get(i);
      assertTrue(entity.getEntity() instanceof Form);
      assertEquals(MediaType.APPLICATION_FORM_URLENCODED_TYPE, entity.getMediaType());
      MultivaluedMap<String, String> formParams = ((Form) entity.getEntity()).asMap();
      assertEquals(1, formParams.get("db").size());
      assertEquals("protein", formParams.get("db").get(0));
      int start = i * MAX_IDS_PER_REQUEST;
      List<String> expectedIds = new ArrayList<>(
          proteinIds.subList(start, Math.min(start + MAX_IDS_PER_REQUEST, proteinIds.size())));
      List<String> actualIds = formParams.get("id");
      Collections.sort(expectedIds);
      Collections.sort(actualIds);
      assertEquals(expectedIds, actualIds);
      assertEquals(1, formParams.get("rettype").size());
      assertEquals("fasta", formParams.get("rettype").get(0));
    });
    validateContent(fastas, output);
    assertTrue(message.contains(String.valueOf(fastas.size())));
    assertTrue(message.contains("68"));
  }

  @Test
  public void allProteinMappings_4930_Ncbi() throws Throwable {
    Path esearch = Paths.get(getClass().getResource("/esearch.fcgi.xml").toURI());
    List<String> fastas = new ArrayList<>();
    IntStream.range(0, FETCH_COUNT).forEach(i -> {
      int start = i * MAX_IDS_PER_REQUEST;
      fastas.add(generateFastaSequences(
          proteinIds.subList(start, Math.min(start + MAX_IDS_PER_REQUEST, proteinIds.size()))));
    });
    when(organism.getId()).thenReturn(4930);
    final Path output = temporaryFolder.newFile("output.fasta").toPath();
    when(restClientFactory.createClient()).thenReturn(clientSearch, clientFetchIds,
        clientFetchFasta);
    when(invocationSearch.get(InputStream.class)).thenAnswer(i -> Files.newInputStream(esearch));
    InputStream firstIdsReturn = new ByteArrayInputStream(proteinIds.stream()
        .limit(MAX_IDS_PER_REQUEST).collect(Collectors.joining("\n")).getBytes(FASTA_CHARSET));
    InputStream[] idsReturns = new InputStream[FETCH_COUNT - 1];
    IntStream.range(1, FETCH_COUNT).forEach(i -> {
      try {
        String accesssionsAsString = proteinIds.stream().skip(i * MAX_IDS_PER_REQUEST)
            .limit(MAX_IDS_PER_REQUEST).collect(Collectors.joining("\n"));
        idsReturns[i - 1] = new ByteArrayInputStream(accesssionsAsString.getBytes(FASTA_CHARSET));
      } catch (Exception e) {
        throw new IllegalStateException();
      }
    });
    when(invocationFetchIds.get(eq(InputStream.class))).thenReturn(firstIdsReturn, idsReturns);
    Path releaseNumber = Paths.get(getClass().getResource("/releaseNumber.txt").toURI());
    when(ftpService.localFile(releaseNumberUrl)).thenReturn(releaseNumber);
    InputStream[] fastaReturns = new InputStream[FETCH_COUNT - 1];
    IntStream.range(1, FETCH_COUNT).forEach(i -> {
      try {
        fastaReturns[i - 1] = new ByteArrayInputStream(fastas.get(i).getBytes(FASTA_CHARSET));
      } catch (Exception e) {
        throw new IllegalStateException();
      }
    });
    when(invocationFetchFasta.post(any(), eq(InputStream.class)))
        .thenReturn(new ByteArrayInputStream(fastas.get(0).getBytes(FASTA_CHARSET)), fastaReturns);
    DownloadParametersBean parameters = new DownloadParametersBean();
    parameters.organism(organism);
    parameters.proteinDatabase(ProteinDatabase.NCBI);

    final String message =
        downloadServiceEutils.downloadProteinDatabase(parameters, output, progressBar, locale);

    verify(restClientFactory, times(FETCH_COUNT + 2)).createClient();
    verify(clientSearch).property(ClientProperties.CONNECT_TIMEOUT,
        DownloadServiceAdditionalSequences.REST_TIMEOUT);
    verify(clientSearch).target(eutilsUrl);
    verify(targetSearch).path("esearch.fcgi");
    verify(targetSearch).queryParam("db", "protein");
    verify(targetSearch).queryParam("term", "txid4930[Organism]");
    verify(targetSearch).queryParam("usehistory", "y");
    verify(targetSearch).request();
    verify(invocationSearch).get(InputStream.class);
    verify(clientFetchIds).property(ClientProperties.CONNECT_TIMEOUT,
        DownloadServiceAdditionalSequences.REST_TIMEOUT);
    verify(clientFetchIds).target(eutilsUrl);
    verify(ftpService, never()).anonymousConnect(ftpUrl);
    verify(ftpService, never()).localFile(releaseNumberUrl);
    verify(ftpService, never()).downloadFile(ftpClient, releaseNumberUrl, releaseNumber,
        progressBar, locale);
    verify(targetFetchIds).path("efetch.fcgi");
    verify(targetFetchIds).queryParam("db", "protein");
    verify(targetFetchIds).queryParam("WebEnv",
        "NCID_1_174988986_130.14.22.215_9001_1434470567_1270571334_0MetA0_S_MegaStore_F_1");
    verify(targetFetchIds).queryParam("query_key", "1");
    verify(targetFetchIds).queryParam("rettype", "acc");
    verify(targetFetchIds).queryParam("retmax", MAX_IDS_PER_REQUEST);
    IntStream.range(0, FETCH_COUNT).forEach(i -> {
      verify(targetFetchIds).queryParam("retstart", i * MAX_IDS_PER_REQUEST);
    });
    verify(targetFetchIds, times(FETCH_COUNT)).request();
    verify(invocationFetchIds, times(FETCH_COUNT)).get(InputStream.class);
    verify(clientFetchFasta, times(FETCH_COUNT)).property(ClientProperties.CONNECT_TIMEOUT,
        DownloadServiceAdditionalSequences.REST_TIMEOUT);
    verify(clientFetchFasta, times(FETCH_COUNT)).target(eutilsUrl);
    verify(targetFetchFasta, times(FETCH_COUNT)).path("efetch.fcgi");
    verify(targetFetchFasta, times(FETCH_COUNT)).request();
    verify(invocationFetchFasta, times(FETCH_COUNT)).post(entityCaptor.capture(),
        eq(InputStream.class));
    IntStream.range(0, FETCH_COUNT).forEach(i -> {
      Entity<?> entity = entityCaptor.getAllValues().get(i);
      assertTrue(entity.getEntity() instanceof Form);
      assertEquals(MediaType.APPLICATION_FORM_URLENCODED_TYPE, entity.getMediaType());
      MultivaluedMap<String, String> formParams = ((Form) entity.getEntity()).asMap();
      assertEquals(1, formParams.get("db").size());
      assertEquals("protein", formParams.get("db").get(0));
      int start = i * MAX_IDS_PER_REQUEST;
      List<String> expectedIds = new ArrayList<>(
          proteinIds.subList(start, Math.min(start + MAX_IDS_PER_REQUEST, proteinIds.size())));
      List<String> actualIds = formParams.get("id");
      Collections.sort(expectedIds);
      Collections.sort(actualIds);
      assertEquals(expectedIds, actualIds);
      assertEquals(1, formParams.get("rettype").size());
      assertEquals("fasta", formParams.get("rettype").get(0));
    });
    validateContent(fastas, output);
    assertTrue(message.contains(String.valueOf(fastas.size())));
  }

  @Test
  public void allProteinMappings_NonFasta() throws Throwable {
    Path esearch = Paths.get(getClass().getResource("/esearch.fcgi.xml").toURI());
    final Path efetchIds = Paths.get(getClass().getResource("/accessions.txt").toURI());
    List<String> fastas = new ArrayList<>();
    IntStream.range(0, FETCH_COUNT).forEach(i -> {
      int start = i * MAX_IDS_PER_REQUEST;
      fastas.add(generateFastaSequences(
          proteinIds.subList(start, Math.min(start + MAX_IDS_PER_REQUEST, proteinIds.size()))));
    });
    when(organism.getId()).thenReturn(9606);
    final Path output = temporaryFolder.newFile("output.fasta").toPath();
    when(restClientFactory.createClient()).thenReturn(clientSearch, clientFetchIds,
        clientFetchFasta);
    when(invocationSearch.get(InputStream.class)).thenAnswer(i -> Files.newInputStream(esearch));
    InputStream firstIdsReturn = new ByteArrayInputStream(proteinIds.stream()
        .limit(MAX_IDS_PER_REQUEST).collect(Collectors.joining("\n")).getBytes(FASTA_CHARSET));
    InputStream[] idsReturns = new InputStream[FETCH_COUNT - 1];
    IntStream.range(1, FETCH_COUNT).forEach(i -> {
      try {
        String accesssionsAsString = proteinIds.stream().skip(i * MAX_IDS_PER_REQUEST)
            .limit(MAX_IDS_PER_REQUEST).collect(Collectors.joining("\n"));
        idsReturns[i - 1] = new ByteArrayInputStream(accesssionsAsString.getBytes(FASTA_CHARSET));
      } catch (Exception e) {
        throw new IllegalStateException();
      }
    });
    when(invocationFetchIds.get(eq(InputStream.class))).thenReturn(firstIdsReturn, idsReturns);
    Path releaseNumber = Paths.get(getClass().getResource("/releaseNumber.txt").toURI());
    when(ftpService.localFile(releaseNumberUrl)).thenReturn(releaseNumber);
    InputStream[] fastaReturns = new InputStream[FETCH_COUNT];
    fastaReturns[0] = new ByteArrayInputStream(parseContent(efetchIds).getBytes(FASTA_CHARSET));
    IntStream.range(1, FETCH_COUNT).forEach(i -> {
      try {
        fastaReturns[i] = new ByteArrayInputStream(fastas.get(i).getBytes(FASTA_CHARSET));
      } catch (Exception e) {
        throw new IllegalStateException();
      }
    });
    when(invocationFetchFasta.post(any(), eq(InputStream.class)))
        .thenReturn(new ByteArrayInputStream(fastas.get(0).getBytes(FASTA_CHARSET)), fastaReturns);
    DownloadParametersBean parameters = new DownloadParametersBean();
    parameters.organism(organism);
    parameters.proteinDatabase(ProteinDatabase.REFSEQ);

    final String message =
        downloadServiceEutils.downloadProteinDatabase(parameters, output, progressBar, locale);

    verify(restClientFactory, times(FETCH_COUNT + 3)).createClient();
    verify(clientSearch).property(ClientProperties.CONNECT_TIMEOUT,
        DownloadServiceAdditionalSequences.REST_TIMEOUT);
    verify(clientSearch).target(eutilsUrl);
    verify(targetSearch).path("esearch.fcgi");
    verify(targetSearch).queryParam("db", "protein");
    verify(targetSearch).queryParam("term", "txid9606[Organism] AND refseq[filter]");
    verify(targetSearch).queryParam("usehistory", "y");
    verify(targetSearch).request();
    verify(invocationSearch).get(InputStream.class);
    verify(clientFetchIds).property(ClientProperties.CONNECT_TIMEOUT,
        DownloadServiceAdditionalSequences.REST_TIMEOUT);
    verify(clientFetchIds).target(eutilsUrl);
    verify(ftpService).anonymousConnect(ftpUrl);
    verify(ftpService).localFile(releaseNumberUrl);
    verify(ftpService).downloadFile(ftpClient, releaseNumberUrl, releaseNumber, progressBar,
        locale);
    verify(targetFetchIds).path("efetch.fcgi");
    verify(targetFetchIds).queryParam("db", "protein");
    verify(targetFetchIds).queryParam("WebEnv",
        "NCID_1_174988986_130.14.22.215_9001_1434470567_1270571334_0MetA0_S_MegaStore_F_1");
    verify(targetFetchIds).queryParam("query_key", "1");
    verify(targetFetchIds).queryParam("rettype", "acc");
    verify(targetFetchIds).queryParam("retmax", MAX_IDS_PER_REQUEST);
    IntStream.range(0, FETCH_COUNT).forEach(i -> {
      verify(targetFetchIds).queryParam("retstart", i * MAX_IDS_PER_REQUEST);
    });
    verify(targetFetchIds, times(FETCH_COUNT)).request();
    verify(invocationFetchIds, times(FETCH_COUNT)).get(InputStream.class);
    verify(clientFetchFasta, times(FETCH_COUNT + 1)).property(ClientProperties.CONNECT_TIMEOUT,
        DownloadServiceAdditionalSequences.REST_TIMEOUT);
    verify(clientFetchFasta, times(FETCH_COUNT + 1)).target(eutilsUrl);
    verify(targetFetchFasta, times(FETCH_COUNT + 1)).path("efetch.fcgi");
    verify(targetFetchFasta, times(FETCH_COUNT + 1)).request();
    verify(invocationFetchFasta, times(FETCH_COUNT + 1)).post(entityCaptor.capture(),
        eq(InputStream.class));
    IntStream.range(0, FETCH_COUNT + 1).forEach(i -> {
      int iminusone = i;
      if (iminusone > 1) {
        iminusone--;
      }
      Entity<?> entity = entityCaptor.getAllValues().get(i);
      assertTrue(entity.getEntity() instanceof Form);
      assertEquals(MediaType.APPLICATION_FORM_URLENCODED_TYPE, entity.getMediaType());
      MultivaluedMap<String, String> formParams = ((Form) entity.getEntity()).asMap();
      assertEquals(1, formParams.get("db").size());
      assertEquals("protein", formParams.get("db").get(0));
      int start = iminusone * MAX_IDS_PER_REQUEST;
      List<String> expectedIds = new ArrayList<>(
          proteinIds.subList(start, Math.min(start + MAX_IDS_PER_REQUEST, proteinIds.size())));
      List<String> actualIds = formParams.get("id");
      Collections.sort(expectedIds);
      Collections.sort(actualIds);
      assertEquals(expectedIds, actualIds);
      assertEquals(1, formParams.get("rettype").size());
      assertEquals("fasta", formParams.get("rettype").get(0));
    });
    validateContent(fastas, output);
    assertTrue(message.contains(String.valueOf(fastas.size())));
    assertTrue(message.contains("68"));
  }

  @Test
  public void allProteinMappings_MissingSequences() throws Throwable {
    Path esearch = Paths.get(getClass().getResource("/esearch.fcgi.xml").toURI());
    List<String> fastas = new ArrayList<>();
    IntStream.range(0, FETCH_COUNT).forEach(i -> {
      int start = i * MAX_IDS_PER_REQUEST;
      fastas.add(generateFastaSequences(
          proteinIds.subList(start, Math.min(start + MAX_IDS_PER_REQUEST, proteinIds.size()))));
    });
    when(organism.getId()).thenReturn(9606);
    final Path output = temporaryFolder.newFile("output.fasta").toPath();
    when(restClientFactory.createClient()).thenReturn(clientSearch, clientFetchIds,
        clientFetchFasta);
    when(invocationSearch.get(InputStream.class)).thenAnswer(i -> Files.newInputStream(esearch));
    InputStream firstIdsReturn = new ByteArrayInputStream(proteinIds.stream()
        .limit(MAX_IDS_PER_REQUEST).collect(Collectors.joining("\n")).getBytes(FASTA_CHARSET));
    InputStream[] idsReturns = new InputStream[FETCH_COUNT - 1];
    IntStream.range(1, FETCH_COUNT).forEach(i -> {
      try {
        String accesssionsAsString = proteinIds.stream().skip(i * MAX_IDS_PER_REQUEST)
            .limit(MAX_IDS_PER_REQUEST).collect(Collectors.joining("\n"));
        idsReturns[i - 1] = new ByteArrayInputStream(accesssionsAsString.getBytes(FASTA_CHARSET));
      } catch (Exception e) {
        throw new IllegalStateException();
      }
    });
    when(invocationFetchIds.get(eq(InputStream.class))).thenReturn(firstIdsReturn, idsReturns);
    Path releaseNumber = Paths.get(getClass().getResource("/releaseNumber.txt").toURI());
    when(ftpService.localFile(releaseNumberUrl)).thenReturn(releaseNumber);
    InputStream[] fastaReturns = new InputStream[FETCH_COUNT];
    fastaReturns[0] = new ByteArrayInputStream(
        generateFastaSequences(proteinIds.subList(MAX_IDS_PER_REQUEST, MAX_IDS_PER_REQUEST * 2 - 5))
            .getBytes(FASTA_CHARSET));
    IntStream.range(1, FETCH_COUNT).forEach(i -> {
      try {
        fastaReturns[i] = new ByteArrayInputStream(fastas.get(i).getBytes(FASTA_CHARSET));
      } catch (Exception e) {
        throw new IllegalStateException();
      }
    });
    when(invocationFetchFasta.post(any(), eq(InputStream.class)))
        .thenReturn(new ByteArrayInputStream(fastas.get(0).getBytes(FASTA_CHARSET)), fastaReturns);
    DownloadParametersBean parameters = new DownloadParametersBean();
    parameters.organism(organism);
    parameters.proteinDatabase(ProteinDatabase.REFSEQ);

    final String message =
        downloadServiceEutils.downloadProteinDatabase(parameters, output, progressBar, locale);

    verify(restClientFactory, times(FETCH_COUNT + 3)).createClient();
    verify(clientSearch).property(ClientProperties.CONNECT_TIMEOUT,
        DownloadServiceAdditionalSequences.REST_TIMEOUT);
    verify(clientSearch).target(eutilsUrl);
    verify(targetSearch).path("esearch.fcgi");
    verify(targetSearch).queryParam("db", "protein");
    verify(targetSearch).queryParam("term", "txid9606[Organism] AND refseq[filter]");
    verify(targetSearch).queryParam("usehistory", "y");
    verify(targetSearch).request();
    verify(invocationSearch).get(InputStream.class);
    verify(clientFetchIds).property(ClientProperties.CONNECT_TIMEOUT,
        DownloadServiceAdditionalSequences.REST_TIMEOUT);
    verify(clientFetchIds).target(eutilsUrl);
    verify(ftpService).anonymousConnect(ftpUrl);
    verify(ftpService).localFile(releaseNumberUrl);
    verify(ftpService).downloadFile(ftpClient, releaseNumberUrl, releaseNumber, progressBar,
        locale);
    verify(targetFetchIds).path("efetch.fcgi");
    verify(targetFetchIds).queryParam("db", "protein");
    verify(targetFetchIds).queryParam("WebEnv",
        "NCID_1_174988986_130.14.22.215_9001_1434470567_1270571334_0MetA0_S_MegaStore_F_1");
    verify(targetFetchIds).queryParam("query_key", "1");
    verify(targetFetchIds).queryParam("rettype", "acc");
    verify(targetFetchIds).queryParam("retmax", MAX_IDS_PER_REQUEST);
    IntStream.range(0, FETCH_COUNT).forEach(i -> {
      verify(targetFetchIds).queryParam("retstart", i * MAX_IDS_PER_REQUEST);
    });
    verify(targetFetchIds, times(FETCH_COUNT)).request();
    verify(invocationFetchIds, times(FETCH_COUNT)).get(InputStream.class);
    verify(clientFetchFasta, times(FETCH_COUNT + 1)).property(ClientProperties.CONNECT_TIMEOUT,
        DownloadServiceAdditionalSequences.REST_TIMEOUT);
    verify(clientFetchFasta, times(FETCH_COUNT + 1)).target(eutilsUrl);
    verify(clientFetchFasta, times(FETCH_COUNT + 1)).target(eutilsUrl);
    verify(targetFetchFasta, times(FETCH_COUNT + 1)).path("efetch.fcgi");
    verify(targetFetchFasta, times(FETCH_COUNT + 1)).request();
    verify(invocationFetchFasta, times(FETCH_COUNT + 1)).post(entityCaptor.capture(),
        eq(InputStream.class));
    IntStream.range(0, FETCH_COUNT + 1).forEach(i -> {
      int iminusone = i;
      if (iminusone > 1) {
        iminusone--;
      }
      Entity<?> entity = entityCaptor.getAllValues().get(i);
      assertTrue(entity.getEntity() instanceof Form);
      assertEquals(MediaType.APPLICATION_FORM_URLENCODED_TYPE, entity.getMediaType());
      MultivaluedMap<String, String> formParams = ((Form) entity.getEntity()).asMap();
      assertEquals(1, formParams.get("db").size());
      assertEquals("protein", formParams.get("db").get(0));
      int start = iminusone * MAX_IDS_PER_REQUEST;
      List<String> expectedIds = new ArrayList<>(
          proteinIds.subList(start, Math.min(start + MAX_IDS_PER_REQUEST, proteinIds.size())));
      List<String> actualIds = formParams.get("id");
      Collections.sort(expectedIds);
      Collections.sort(actualIds);
      assertEquals(expectedIds, actualIds);
      assertEquals(1, formParams.get("rettype").size());
      assertEquals("fasta", formParams.get("rettype").get(0));
    });
    validateContent(fastas, output);
    assertTrue(message.contains(String.valueOf(fastas.size())));
    assertTrue(message.contains("68"));
  }

  @Test
  public void allProteinMappings_ProteinDatabase_NotSupported() throws Throwable {
    Path esearch = Paths.get(getClass().getResource("/esearch.fcgi.xml").toURI());
    List<String> fastas = new ArrayList<>();
    IntStream.range(0, FETCH_COUNT).forEach(i -> {
      int start = i * MAX_IDS_PER_REQUEST;
      fastas.add(generateFastaSequences(
          proteinIds.subList(start, Math.min(start + MAX_IDS_PER_REQUEST, proteinIds.size()))));
    });
    when(organism.getId()).thenReturn(9606);
    Path output = temporaryFolder.newFile("output.fasta").toPath();
    when(restClientFactory.createClient()).thenReturn(clientSearch, clientFetchIds,
        clientFetchFasta);
    when(invocationSearch.get(InputStream.class)).thenAnswer(i -> Files.newInputStream(esearch));
    InputStream firstIdsReturn = new ByteArrayInputStream(proteinIds.stream()
        .limit(MAX_IDS_PER_REQUEST).collect(Collectors.joining("\n")).getBytes(FASTA_CHARSET));
    InputStream[] idsReturns = new InputStream[FETCH_COUNT - 1];
    IntStream.range(1, FETCH_COUNT).forEach(i -> {
      try {
        String accesssionsAsString = proteinIds.stream().skip(i * MAX_IDS_PER_REQUEST)
            .limit(MAX_IDS_PER_REQUEST).collect(Collectors.joining("\n"));
        idsReturns[i - 1] = new ByteArrayInputStream(accesssionsAsString.getBytes(FASTA_CHARSET));
      } catch (Exception e) {
        throw new IllegalStateException();
      }
    });
    when(invocationFetchIds.get(eq(InputStream.class))).thenReturn(firstIdsReturn, idsReturns);
    InputStream[] fastaReturns = new InputStream[FETCH_COUNT - 1];
    IntStream.range(1, FETCH_COUNT).forEach(i -> {
      try {
        fastaReturns[i - 1] = new ByteArrayInputStream(fastas.get(i).getBytes(FASTA_CHARSET));
      } catch (Exception e) {
        throw new IllegalStateException();
      }
    });
    when(invocationFetchFasta.post(any(), eq(InputStream.class)))
        .thenReturn(new ByteArrayInputStream(fastas.get(0).getBytes(FASTA_CHARSET)), fastaReturns);
    DownloadParametersBean parameters = new DownloadParametersBean();
    parameters.organism(organism);
    parameters.proteinDatabase(ProteinDatabase.UNIPROT);

    try {
      downloadServiceEutils.downloadProteinDatabase(parameters, output, progressBar, locale);
      fail("Expected IllegalArgumentException");
    } catch (IllegalArgumentException e) {
      // Success.
    }
  }

  @Test
  public void allProteinMappings_Prohits() throws Throwable {
    parseProteinIds(Paths.get(getClass().getResource("/accessions1.txt").toURI()));
    Path esearch = Paths.get(getClass().getResource("/esearch1.fcgi.xml").toURI());
    List<String> fastas = new ArrayList<>();
    IntStream.range(0, 1).forEach(i -> {
      int start = i * MAX_IDS_PER_REQUEST;
      fastas.add(generateFastaSequences(
          proteinIds.subList(start, Math.min(start + MAX_IDS_PER_REQUEST, proteinIds.size()))));
    });
    when(organism.getId()).thenReturn(9606);
    final Path output = temporaryFolder.newFile("output.fasta").toPath();
    when(restClientFactory.createClient()).thenReturn(clientSearch, clientFetchIds,
        clientFetchFasta);
    when(invocationSearch.get(InputStream.class)).thenAnswer(i -> Files.newInputStream(esearch));
    InputStream idsReturn = new ByteArrayInputStream(proteinIds.stream().limit(MAX_IDS_PER_REQUEST)
        .collect(Collectors.joining("\n")).getBytes(FASTA_CHARSET));
    when(invocationFetchIds.get(eq(InputStream.class))).thenReturn(idsReturn);
    Path releaseNumber = Paths.get(getClass().getResource("/releaseNumber.txt").toURI());
    when(ftpService.localFile(releaseNumberUrl)).thenReturn(releaseNumber);
    InputStream[] fastaReturns = new InputStream[1 - 1];
    IntStream.range(1, 1).forEach(i -> {
      try {
        fastaReturns[i - 1] = new ByteArrayInputStream(fastas.get(i).getBytes(FASTA_CHARSET));
      } catch (Exception e) {
        throw new IllegalStateException();
      }
    });
    when(invocationFetchFasta.post(any(), eq(InputStream.class)))
        .thenReturn(new ByteArrayInputStream(fastas.get(0).getBytes(FASTA_CHARSET)), fastaReturns);
    DownloadParametersBean parameters = new DownloadParametersBean();
    parameters.organism(organism);
    parameters.proteinDatabase(ProteinDatabase.REFSEQ);
    parameters.prohits(true);

    final String message =
        downloadServiceEutils.downloadProteinDatabase(parameters, output, progressBar, locale);

    verify(restClientFactory, times(3)).createClient();
    verify(clientSearch).property(ClientProperties.CONNECT_TIMEOUT,
        DownloadServiceAdditionalSequences.REST_TIMEOUT);
    verify(clientSearch).target(eutilsUrl);
    verify(targetSearch).path("esearch.fcgi");
    verify(targetSearch).queryParam("db", "protein");
    verify(targetSearch).queryParam("term", "txid9606[Organism] AND refseq[filter]");
    verify(targetSearch).queryParam("usehistory", "y");
    verify(targetSearch).request();
    verify(invocationSearch).get(InputStream.class);
    verify(clientFetchIds).property(ClientProperties.CONNECT_TIMEOUT,
        DownloadServiceAdditionalSequences.REST_TIMEOUT);
    verify(clientFetchIds).target(eutilsUrl);
    verify(targetFetchIds).path("efetch.fcgi");
    verify(targetFetchIds).queryParam("db", "protein");
    verify(targetFetchIds).queryParam("WebEnv",
        "NCID_1_174988986_130.14.22.215_9001_1434470567_1270571334_0MetA0_S_MegaStore_F_1");
    verify(targetFetchIds).queryParam("query_key", "1");
    verify(targetFetchIds).queryParam("rettype", "acc");
    verify(targetFetchIds).queryParam("retmax", MAX_IDS_PER_REQUEST);
    verify(targetFetchIds).queryParam("retstart", 0);
    verify(targetFetchIds, times(1)).request();
    verify(invocationFetchIds, times(1)).get(InputStream.class);
    verify(clientFetchFasta, times(1)).property(ClientProperties.CONNECT_TIMEOUT,
        DownloadServiceAdditionalSequences.REST_TIMEOUT);
    verify(clientFetchFasta, times(1)).target(eutilsUrl);
    verify(targetFetchFasta, times(1)).path("efetch.fcgi");
    verify(targetFetchFasta, times(1)).request();
    verify(invocationFetchFasta, times(1)).post(entityCaptor.capture(), eq(InputStream.class));
    IntStream.range(0, 1).forEach(i -> {
      Entity<?> entity = entityCaptor.getAllValues().get(i);
      assertTrue(entity.getEntity() instanceof Form);
      assertEquals(MediaType.APPLICATION_FORM_URLENCODED_TYPE, entity.getMediaType());
      MultivaluedMap<String, String> formParams = ((Form) entity.getEntity()).asMap();
      assertEquals(1, formParams.get("db").size());
      assertEquals("protein", formParams.get("db").get(0));
      int start = i * MAX_IDS_PER_REQUEST;
      List<String> expectedIds = new ArrayList<>(
          proteinIds.subList(start, Math.min(start + MAX_IDS_PER_REQUEST, proteinIds.size())));
      List<String> actualIds = formParams.get("id");
      Collections.sort(expectedIds);
      Collections.sort(actualIds);
      assertEquals(expectedIds, actualIds);
      assertEquals(1, formParams.get("rettype").size());
      assertEquals("fasta", formParams.get("rettype").get(0));
    });
    List<String> fastasWithProhits = new ArrayList<>(fastas);
    fastasWithProhits.add(new String(
        Files.readAllBytes(Paths.get(getClass().getResource("/prohits_additions.fasta").toURI())),
        FASTA_CHARSET));
    validateContent(fastasWithProhits, output);
    assertTrue(message.contains(String.valueOf(fastas.size())));
    assertTrue(message.contains("68"));
  }

  @Test
  @RetryOnFail(5)
  public void allProteinMappings_Decoy() throws Throwable {
    final Path esearch = Paths.get(getClass().getResource("/esearch.fcgi.xml").toURI());
    List<String> fastas = new ArrayList<>();
    IntStream.range(0, FETCH_COUNT).forEach(i -> {
      int start = i * MAX_IDS_PER_REQUEST;
      fastas.add(generateFastaSequences(
          proteinIds.subList(start, Math.min(start + MAX_IDS_PER_REQUEST, proteinIds.size()))));
    });
    when(organism.getId()).thenReturn(9606);
    Files.deleteIfExists(temporaryFolder.getRoot().toPath().resolve("output.fasta"));
    final Path output = temporaryFolder.newFile("output.fasta").toPath();
    when(restClientFactory.createClient()).thenReturn(clientSearch, clientFetchIds,
        clientFetchFasta);
    when(invocationSearch.get(InputStream.class)).thenAnswer(i -> Files.newInputStream(esearch));
    InputStream firstIdsReturn = new ByteArrayInputStream(proteinIds.stream()
        .limit(MAX_IDS_PER_REQUEST).collect(Collectors.joining("\n")).getBytes(FASTA_CHARSET));
    InputStream[] idsReturns = new InputStream[FETCH_COUNT - 1];
    IntStream.range(1, FETCH_COUNT).forEach(i -> {
      try {
        String accesssionsAsString = proteinIds.stream().skip(i * MAX_IDS_PER_REQUEST)
            .limit(MAX_IDS_PER_REQUEST).collect(Collectors.joining("\n"));
        idsReturns[i - 1] = new ByteArrayInputStream(accesssionsAsString.getBytes(FASTA_CHARSET));
      } catch (Exception e) {
        throw new IllegalStateException();
      }
    });
    when(invocationFetchIds.get(eq(InputStream.class))).thenReturn(firstIdsReturn, idsReturns);
    Path releaseNumber = Paths.get(getClass().getResource("/releaseNumber.txt").toURI());
    when(ftpService.localFile(releaseNumberUrl)).thenReturn(releaseNumber);
    InputStream[] fastaReturns = new InputStream[FETCH_COUNT - 1];
    IntStream.range(1, FETCH_COUNT).forEach(i -> {
      try {
        fastaReturns[i - 1] = new ByteArrayInputStream(fastas.get(i).getBytes(FASTA_CHARSET));
      } catch (Exception e) {
        throw new IllegalStateException();
      }
    });
    when(invocationFetchFasta.post(any(), eq(InputStream.class)))
        .thenReturn(new ByteArrayInputStream(fastas.get(0).getBytes(FASTA_CHARSET)), fastaReturns);
    DownloadParametersBean parameters = new DownloadParametersBean();
    parameters.organism(organism);
    parameters.proteinDatabase(ProteinDatabase.REFSEQ);
    parameters.decoy(true);

    final String message =
        downloadServiceEutils.downloadProteinDatabase(parameters, output, progressBar, locale);

    for (int aa = 'A'; aa <= 'Z'; aa++) {
      int[] counts = count(aa, output);
      int max = Math.max(counts[0], counts[1]);
      int min = Math.min(counts[0], counts[1]);
      double diff;
      if (max == 0) {
        diff = 1.0;
      } else {
        diff = (double) min / max;
      }
      assertEquals(1.0, diff, 0.1);
    }
    assertTrue(message.contains(String.valueOf(fastas.size())));
    assertTrue(message.contains("68"));
  }

  @Test
  public void allProteinMappings_OutputExists_AllValidSequences() throws Throwable {
    Path esearch = Paths.get(getClass().getResource("/esearch.fcgi.xml").toURI());
    List<String> fastas = new ArrayList<>();
    IntStream.range(0, FETCH_COUNT).forEach(i -> {
      int start = i * MAX_IDS_PER_REQUEST;
      fastas.add(generateFastaSequences(
          proteinIds.subList(start, Math.min(start + MAX_IDS_PER_REQUEST, proteinIds.size()))));
    });
    when(organism.getId()).thenReturn(9606);
    final Path output = temporaryFolder.newFile("output.fasta").toPath();
    when(restClientFactory.createClient()).thenReturn(clientSearch, clientFetchIds,
        clientFetchFasta);
    when(invocationSearch.get(InputStream.class)).thenAnswer(i -> Files.newInputStream(esearch));
    InputStream firstIdsReturn = new ByteArrayInputStream(proteinIds.stream()
        .limit(MAX_IDS_PER_REQUEST).collect(Collectors.joining("\n")).getBytes(FASTA_CHARSET));
    InputStream[] idsReturns = new InputStream[FETCH_COUNT - 1];
    IntStream.range(1, FETCH_COUNT).forEach(i -> {
      try {
        String accesssionsAsString = proteinIds.stream().skip(i * MAX_IDS_PER_REQUEST)
            .limit(MAX_IDS_PER_REQUEST).collect(Collectors.joining("\n"));
        idsReturns[i - 1] = new ByteArrayInputStream(accesssionsAsString.getBytes(FASTA_CHARSET));
      } catch (Exception e) {
        throw new IllegalStateException();
      }
    });
    when(invocationFetchIds.get(eq(InputStream.class))).thenReturn(firstIdsReturn, idsReturns);
    Path releaseNumber = Paths.get(getClass().getResource("/releaseNumber.txt").toURI());
    when(ftpService.localFile(releaseNumberUrl)).thenReturn(releaseNumber);
    InputStream[] fastaReturns = new InputStream[FETCH_COUNT - 2];
    IntStream.range(2, FETCH_COUNT).forEach(i -> {
      try {
        fastaReturns[i - 2] = new ByteArrayInputStream(fastas.get(i).getBytes(FASTA_CHARSET));
      } catch (Exception e) {
        throw new IllegalStateException();
      }
    });
    when(invocationFetchFasta.post(any(), eq(InputStream.class)))
        .thenReturn(new ByteArrayInputStream(fastas.get(1).getBytes(FASTA_CHARSET)), fastaReturns);
    DownloadParametersBean parameters = new DownloadParametersBean();
    parameters.organism(organism);
    parameters.proteinDatabase(ProteinDatabase.REFSEQ);
    try (BufferedWriter writer = Files.newBufferedWriter(output, FASTA_CHARSET)) {
      writer.write(fastas.get(0));
    }

    downloadServiceEutils.downloadProteinDatabase(parameters, output, progressBar, locale);

    verify(restClientFactory, times(FETCH_COUNT + 1)).createClient();
    verify(clientSearch).property(ClientProperties.CONNECT_TIMEOUT,
        DownloadServiceAdditionalSequences.REST_TIMEOUT);
    verify(clientSearch).target(eutilsUrl);
    verify(targetSearch).path("esearch.fcgi");
    verify(targetSearch).queryParam("db", "protein");
    verify(targetSearch).queryParam("term", "txid9606[Organism] AND refseq[filter]");
    verify(targetSearch).queryParam("usehistory", "y");
    verify(targetSearch).request();
    verify(invocationSearch).get(InputStream.class);
    verify(clientFetchIds).property(ClientProperties.CONNECT_TIMEOUT,
        DownloadServiceAdditionalSequences.REST_TIMEOUT);
    verify(clientFetchIds).target(eutilsUrl);
    verify(targetFetchIds).path("efetch.fcgi");
    verify(targetFetchIds).queryParam("db", "protein");
    verify(targetFetchIds).queryParam("WebEnv",
        "NCID_1_174988986_130.14.22.215_9001_1434470567_1270571334_0MetA0_S_MegaStore_F_1");
    verify(targetFetchIds).queryParam("query_key", "1");
    verify(targetFetchIds).queryParam("rettype", "acc");
    verify(targetFetchIds).queryParam("retmax", MAX_IDS_PER_REQUEST);
    IntStream.range(0, FETCH_COUNT).forEach(i -> {
      verify(targetFetchIds).queryParam("retstart", i * MAX_IDS_PER_REQUEST);
    });
    verify(targetFetchIds, times(FETCH_COUNT)).request();
    verify(invocationFetchIds, times(FETCH_COUNT)).get(InputStream.class);
    verify(clientFetchFasta, times(FETCH_COUNT - 1)).property(ClientProperties.CONNECT_TIMEOUT,
        DownloadServiceAdditionalSequences.REST_TIMEOUT);
    verify(clientFetchFasta, times(FETCH_COUNT - 1)).target(eutilsUrl);
    verify(targetFetchFasta, times(FETCH_COUNT - 1)).path("efetch.fcgi");
    verify(targetFetchFasta, times(FETCH_COUNT - 1)).request();
    verify(invocationFetchFasta, times(FETCH_COUNT - 1)).post(entityCaptor.capture(),
        eq(InputStream.class));
    for (int i = 1; i < FETCH_COUNT; i++) {
      Entity<?> entity = entityCaptor.getAllValues().get(i - 1);
      assertTrue(entity.getEntity() instanceof Form);
      assertEquals(MediaType.APPLICATION_FORM_URLENCODED_TYPE, entity.getMediaType());
      MultivaluedMap<String, String> formParams = ((Form) entity.getEntity()).asMap();
      assertEquals(1, formParams.get("db").size());
      assertEquals("protein", formParams.get("db").get(0));
      int start = i * MAX_IDS_PER_REQUEST;
      List<String> expectedIds = new ArrayList<>(
          proteinIds.subList(start, Math.min(start + MAX_IDS_PER_REQUEST, proteinIds.size())));
      List<String> actualIds = formParams.get("id");
      Collections.sort(expectedIds);
      Collections.sort(actualIds);
      assertEquals(expectedIds, actualIds);
      assertEquals(1, formParams.get("rettype").size());
      assertEquals("fasta", formParams.get("rettype").get(0));
    }
    validateContent(fastas, output);
  }

  @Test
  public void allProteinMappings_OutputExists_SomeInvalidSequences() throws Throwable {
    Path esearch = Paths.get(getClass().getResource("/esearch.fcgi.xml").toURI());
    List<String> fastas = new ArrayList<>();
    IntStream.range(0, FETCH_COUNT).forEach(i -> {
      int start = i * MAX_IDS_PER_REQUEST;
      fastas.add(generateFastaSequences(
          proteinIds.subList(start, Math.min(start + MAX_IDS_PER_REQUEST, proteinIds.size()))));
    });
    when(organism.getId()).thenReturn(9606);
    final Path output = temporaryFolder.newFile("output.fasta").toPath();
    when(restClientFactory.createClient()).thenReturn(clientSearch, clientFetchIds,
        clientFetchFasta);
    when(invocationSearch.get(InputStream.class)).thenAnswer(i -> Files.newInputStream(esearch));
    InputStream firstIdsReturn = new ByteArrayInputStream(proteinIds.stream()
        .limit(MAX_IDS_PER_REQUEST).collect(Collectors.joining("\n")).getBytes(FASTA_CHARSET));
    InputStream[] idsReturns = new InputStream[FETCH_COUNT - 1];
    IntStream.range(1, FETCH_COUNT).forEach(i -> {
      try {
        String accesssionsAsString = proteinIds.stream().skip(i * MAX_IDS_PER_REQUEST)
            .limit(MAX_IDS_PER_REQUEST).collect(Collectors.joining("\n"));
        idsReturns[i - 1] = new ByteArrayInputStream(accesssionsAsString.getBytes(FASTA_CHARSET));
      } catch (Exception e) {
        throw new IllegalStateException();
      }
    });
    when(invocationFetchIds.get(eq(InputStream.class))).thenReturn(firstIdsReturn, idsReturns);
    Path releaseNumber = Paths.get(getClass().getResource("/releaseNumber.txt").toURI());
    when(ftpService.localFile(releaseNumberUrl)).thenReturn(releaseNumber);
    InputStream[] fastaReturns = new InputStream[FETCH_COUNT - 2];
    IntStream.range(2, FETCH_COUNT).forEach(i -> {
      try {
        fastaReturns[i - 2] = new ByteArrayInputStream(fastas.get(i).getBytes(FASTA_CHARSET));
      } catch (Exception e) {
        throw new IllegalStateException();
      }
    });
    when(invocationFetchFasta.post(any(), eq(InputStream.class)))
        .thenReturn(new ByteArrayInputStream(fastas.get(1).getBytes(FASTA_CHARSET)), fastaReturns);
    DownloadParametersBean parameters = new DownloadParametersBean();
    parameters.organism(organism);
    parameters.proteinDatabase(ProteinDatabase.REFSEQ);
    try (BufferedWriter writer = Files.newBufferedWriter(output, FASTA_CHARSET)) {
      writer.write(fastas.get(0));
      writer.write(">gi|1234567890\r\n");
      writer.write("AFEAFEAGHAFOJEAWOFJAGJOEWAJFEOKAFKEWOFK\r\n");
      writer.write(">invalid_sequence\r\n");
      writer.write("FJAOGTEIAJFOEKAGJOWAERJFOEKWAVOWAKFGEOAFAJOFEWA\r\n");
    }

    downloadServiceEutils.downloadProteinDatabase(parameters, output, progressBar, locale);

    verify(restClientFactory, times(FETCH_COUNT + 1)).createClient();
    verify(clientSearch).property(ClientProperties.CONNECT_TIMEOUT,
        DownloadServiceAdditionalSequences.REST_TIMEOUT);
    verify(clientSearch).target(eutilsUrl);
    verify(targetSearch).path("esearch.fcgi");
    verify(targetSearch).queryParam("db", "protein");
    verify(targetSearch).queryParam("term", "txid9606[Organism] AND refseq[filter]");
    verify(targetSearch).queryParam("usehistory", "y");
    verify(targetSearch).request();
    verify(invocationSearch).get(InputStream.class);
    verify(clientFetchIds).property(ClientProperties.CONNECT_TIMEOUT,
        DownloadServiceAdditionalSequences.REST_TIMEOUT);
    verify(clientFetchIds).target(eutilsUrl);
    verify(targetFetchIds).path("efetch.fcgi");
    verify(targetFetchIds).queryParam("db", "protein");
    verify(targetFetchIds).queryParam("WebEnv",
        "NCID_1_174988986_130.14.22.215_9001_1434470567_1270571334_0MetA0_S_MegaStore_F_1");
    verify(targetFetchIds).queryParam("query_key", "1");
    verify(targetFetchIds).queryParam("rettype", "acc");
    verify(targetFetchIds).queryParam("retmax", MAX_IDS_PER_REQUEST);
    IntStream.range(0, FETCH_COUNT).forEach(i -> {
      verify(targetFetchIds).queryParam("retstart", i * MAX_IDS_PER_REQUEST);
    });
    verify(targetFetchIds, times(FETCH_COUNT)).request();
    verify(invocationFetchIds, times(FETCH_COUNT)).get(InputStream.class);
    verify(clientFetchFasta, times(FETCH_COUNT - 1)).property(ClientProperties.CONNECT_TIMEOUT,
        DownloadServiceAdditionalSequences.REST_TIMEOUT);
    verify(clientFetchFasta, times(FETCH_COUNT - 1)).target(eutilsUrl);
    verify(targetFetchFasta, times(FETCH_COUNT - 1)).path("efetch.fcgi");
    verify(targetFetchFasta, times(FETCH_COUNT - 1)).request();
    verify(invocationFetchFasta, times(FETCH_COUNT - 1)).post(entityCaptor.capture(),
        eq(InputStream.class));
    for (int i = 1; i < FETCH_COUNT; i++) {
      Entity<?> entity = entityCaptor.getAllValues().get(i - 1);
      assertTrue(entity.getEntity() instanceof Form);
      assertEquals(MediaType.APPLICATION_FORM_URLENCODED_TYPE, entity.getMediaType());
      MultivaluedMap<String, String> formParams = ((Form) entity.getEntity()).asMap();
      assertEquals(1, formParams.get("db").size());
      assertEquals("protein", formParams.get("db").get(0));
      int start = i * MAX_IDS_PER_REQUEST;
      List<String> expectedIds = new ArrayList<>(
          proteinIds.subList(start, Math.min(start + MAX_IDS_PER_REQUEST, proteinIds.size())));
      List<String> actualIds = formParams.get("id");
      Collections.sort(expectedIds);
      Collections.sort(actualIds);
      assertEquals(expectedIds, actualIds);
      assertEquals(1, formParams.get("rettype").size());
      assertEquals("fasta", formParams.get("rettype").get(0));
    }
    validateContent(fastas, output);
  }

  @Test
  public void allProteinMappings_OutputExists_NotFasta() throws Throwable {
    Path esearch = Paths.get(getClass().getResource("/esearch.fcgi.xml").toURI());
    List<String> fastas = new ArrayList<>();
    IntStream.range(0, FETCH_COUNT).forEach(i -> {
      int start = i * MAX_IDS_PER_REQUEST;
      fastas.add(generateFastaSequences(
          proteinIds.subList(start, Math.min(start + MAX_IDS_PER_REQUEST, proteinIds.size()))));
    });
    when(organism.getId()).thenReturn(9606);
    final Path output = temporaryFolder.newFile("output.fasta").toPath();
    when(restClientFactory.createClient()).thenReturn(clientSearch, clientFetchIds,
        clientFetchFasta);
    when(invocationSearch.get(InputStream.class)).thenAnswer(i -> Files.newInputStream(esearch));
    InputStream firstIdsReturn = new ByteArrayInputStream(proteinIds.stream()
        .limit(MAX_IDS_PER_REQUEST).collect(Collectors.joining("\n")).getBytes(FASTA_CHARSET));
    InputStream[] idsReturns = new InputStream[FETCH_COUNT - 1];
    IntStream.range(1, FETCH_COUNT).forEach(i -> {
      try {
        String accesssionsAsString = proteinIds.stream().skip(i * MAX_IDS_PER_REQUEST)
            .limit(MAX_IDS_PER_REQUEST).collect(Collectors.joining("\n"));
        idsReturns[i - 1] = new ByteArrayInputStream(accesssionsAsString.getBytes(FASTA_CHARSET));
      } catch (Exception e) {
        throw new IllegalStateException();
      }
    });
    when(invocationFetchIds.get(eq(InputStream.class))).thenReturn(firstIdsReturn, idsReturns);
    Path releaseNumber = Paths.get(getClass().getResource("/releaseNumber.txt").toURI());
    when(ftpService.localFile(releaseNumberUrl)).thenReturn(releaseNumber);
    InputStream[] fastaReturns = new InputStream[FETCH_COUNT - 1];
    IntStream.range(1, FETCH_COUNT).forEach(i -> {
      try {
        fastaReturns[i - 1] = new ByteArrayInputStream(fastas.get(i).getBytes(FASTA_CHARSET));
      } catch (Exception e) {
        throw new IllegalStateException();
      }
    });
    when(invocationFetchFasta.post(any(), eq(InputStream.class)))
        .thenReturn(new ByteArrayInputStream(fastas.get(0).getBytes(FASTA_CHARSET)), fastaReturns);
    DownloadParametersBean parameters = new DownloadParametersBean();
    parameters.organism(organism);
    parameters.proteinDatabase(ProteinDatabase.REFSEQ);
    try (BufferedWriter writer = Files.newBufferedWriter(output, FASTA_CHARSET)) {
      writer.write("$*#)Q%*Q#$(T^&$(");
    }

    downloadServiceEutils.downloadProteinDatabase(parameters, output, progressBar, locale);

    verify(restClientFactory, times(FETCH_COUNT + 2)).createClient();
    verify(clientSearch).property(ClientProperties.CONNECT_TIMEOUT,
        DownloadServiceAdditionalSequences.REST_TIMEOUT);
    verify(clientSearch).target(eutilsUrl);
    verify(targetSearch).path("esearch.fcgi");
    verify(targetSearch).queryParam("db", "protein");
    verify(targetSearch).queryParam("term", "txid9606[Organism] AND refseq[filter]");
    verify(targetSearch).queryParam("usehistory", "y");
    verify(targetSearch).request();
    verify(invocationSearch).get(InputStream.class);
    verify(clientFetchIds).property(ClientProperties.CONNECT_TIMEOUT,
        DownloadServiceAdditionalSequences.REST_TIMEOUT);
    verify(clientFetchIds).target(eutilsUrl);
    verify(targetFetchIds).path("efetch.fcgi");
    verify(targetFetchIds).queryParam("db", "protein");
    verify(targetFetchIds).queryParam("WebEnv",
        "NCID_1_174988986_130.14.22.215_9001_1434470567_1270571334_0MetA0_S_MegaStore_F_1");
    verify(targetFetchIds).queryParam("query_key", "1");
    verify(targetFetchIds).queryParam("rettype", "acc");
    verify(targetFetchIds).queryParam("retmax", MAX_IDS_PER_REQUEST);
    IntStream.range(0, FETCH_COUNT).forEach(i -> {
      verify(targetFetchIds).queryParam("retstart", i * MAX_IDS_PER_REQUEST);
    });
    verify(targetFetchIds, times(FETCH_COUNT)).request();
    verify(invocationFetchIds, times(FETCH_COUNT)).get(InputStream.class);
    verify(clientFetchFasta, times(FETCH_COUNT)).property(ClientProperties.CONNECT_TIMEOUT,
        DownloadServiceAdditionalSequences.REST_TIMEOUT);
    verify(clientFetchFasta, times(FETCH_COUNT)).target(eutilsUrl);
    verify(targetFetchFasta, times(FETCH_COUNT)).path("efetch.fcgi");
    verify(targetFetchFasta, times(FETCH_COUNT)).request();
    verify(invocationFetchFasta, times(FETCH_COUNT)).post(entityCaptor.capture(),
        eq(InputStream.class));
    for (int i = 0; i < FETCH_COUNT; i++) {
      Entity<?> entity = entityCaptor.getAllValues().get(i);
      assertTrue(entity.getEntity() instanceof Form);
      assertEquals(MediaType.APPLICATION_FORM_URLENCODED_TYPE, entity.getMediaType());
      MultivaluedMap<String, String> formParams = ((Form) entity.getEntity()).asMap();
      assertEquals(1, formParams.get("db").size());
      assertEquals("protein", formParams.get("db").get(0));
      int start = i * MAX_IDS_PER_REQUEST;
      List<String> expectedIds = new ArrayList<>(
          proteinIds.subList(start, Math.min(start + MAX_IDS_PER_REQUEST, proteinIds.size())));
      List<String> actualIds = formParams.get("id");
      Collections.sort(expectedIds);
      Collections.sort(actualIds);
      assertEquals(expectedIds, actualIds);
      assertEquals(1, formParams.get("rettype").size());
      assertEquals("fasta", formParams.get("rettype").get(0));
    }
    validateContent(fastas, output);
  }

  private int[] count(int aa, Path fasta) throws IOException {
    int[] counts = new int[2];
    int index = -1;
    for (String line : Files.readAllLines(fasta)) {
      if (line.startsWith(";")) {
        continue;
      } else if (line.startsWith(">DECOY")) {
        index = 1;
      } else if (line.startsWith(">")) {
        index = 0;
      } else if (index >= 0) {
        counts[index] += line.chars().filter(l -> l == aa).count();
      }
    }
    return counts;
  }
}
