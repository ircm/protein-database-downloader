/*
 * Copyright (c) 2006 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.pdd.download;

import static org.mockito.Mockito.verify;

import ca.qc.ircm.pdd.organism.Organism;
import ca.qc.ircm.pdd.test.config.ServiceTestAnnotations;
import ca.qc.ircm.progressbar.ProgressBar;
import java.nio.file.Path;
import java.util.Locale;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ServiceTestAnnotations
public class DownloadServiceDispatcherTest {
  private DownloadServiceDispatcher downloadServiceDispatcher;
  @Mock
  private DownloadService ncbiDownloadService;
  @Mock
  private DownloadService uniprotDownloadService;
  @Mock
  private Organism organism;
  @Mock
  private Path output;
  @Mock
  private ProgressBar progressBar;
  private Locale locale;

  /**
   * Before test.
   */
  @Before
  public void beforeTest() {
    downloadServiceDispatcher =
        new DownloadServiceDispatcher(ncbiDownloadService, uniprotDownloadService);
    locale = Locale.getDefault();
  }

  @Test
  public void downloadProteinDatabase_Ncbi() throws Throwable {
    ProteinDatabase proteinDatabase = ProteinDatabase.NCBI;
    DownloadParametersBean parameters = new DownloadParametersBean();
    parameters.organism(organism);
    parameters.proteinDatabase(proteinDatabase);

    downloadServiceDispatcher.downloadProteinDatabase(parameters, output, progressBar, locale);

    verify(ncbiDownloadService).downloadProteinDatabase(parameters, output, progressBar, locale);
  }

  @Test
  public void downloadProteinDatabase_Refseq() throws Throwable {
    ProteinDatabase proteinDatabase = ProteinDatabase.REFSEQ;
    DownloadParametersBean parameters = new DownloadParametersBean();
    parameters.organism(organism);
    parameters.proteinDatabase(proteinDatabase);

    downloadServiceDispatcher.downloadProteinDatabase(parameters, output, progressBar, locale);

    verify(ncbiDownloadService).downloadProteinDatabase(parameters, output, progressBar, locale);
  }

  @Test
  public void downloadProteinDatabase_Uniprot() throws Throwable {
    ProteinDatabase proteinDatabase = ProteinDatabase.UNIPROT;
    DownloadParametersBean parameters = new DownloadParametersBean();
    parameters.organism(organism);
    parameters.proteinDatabase(proteinDatabase);

    downloadServiceDispatcher.downloadProteinDatabase(parameters, output, progressBar, locale);

    verify(uniprotDownloadService).downloadProteinDatabase(parameters, output, progressBar, locale);
  }

  @Test
  public void downloadProteinDatabase_Swissprot() throws Throwable {
    ProteinDatabase proteinDatabase = ProteinDatabase.SWISSPROT;
    DownloadParametersBean parameters = new DownloadParametersBean();
    parameters.organism(organism);
    parameters.proteinDatabase(proteinDatabase);

    downloadServiceDispatcher.downloadProteinDatabase(parameters, output, progressBar, locale);

    verify(uniprotDownloadService).downloadProteinDatabase(parameters, output, progressBar, locale);
  }
}
