/*
 * Copyright (c) 2006 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.pdd;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class NamedComparatorTest {
  private NamedComparator comparator = new NamedComparator();

  private Named named(String name) {
    return () -> name;
  }

  @Test
  public void compare() {
    assertTrue(comparator.compare(named("abc"), named("def")) < 0);
    assertTrue(comparator.compare(named("abc"), named("abc")) == 0);
    assertTrue(comparator.compare(named("def"), named("abc")) > 0);
    assertTrue(comparator.compare(named("def"), named("ghi")) < 0);
    assertTrue(comparator.compare(named("def"), named("def")) == 0);
    assertTrue(comparator.compare(named("ghi"), named("abc")) > 0);
    assertTrue(comparator.compare(named("abc"), named("ABC")) == 0);
    assertTrue(comparator.compare(named("ABC"), named("abc")) == 0);
  }

  @Test
  public void compare_Case() {
    comparator = new NamedComparator(new StringComparator(false));
    assertTrue(comparator.compare(named("abc"), named("def")) < 0);
    assertTrue(comparator.compare(named("abc"), named("abc")) == 0);
    assertTrue(comparator.compare(named("def"), named("abc")) > 0);
    assertTrue(comparator.compare(named("def"), named("ghi")) < 0);
    assertTrue(comparator.compare(named("def"), named("def")) == 0);
    assertTrue(comparator.compare(named("ghi"), named("abc")) > 0);
    assertTrue(comparator.compare(named("abc"), named("ABC")) > 0);
    assertTrue(comparator.compare(named("ABC"), named("abc")) < 0);
  }

  @Test
  public void compare_IgnoreCase() {
    comparator = new NamedComparator(new StringComparator(true));
    assertTrue(comparator.compare(named("abc"), named("def")) < 0);
    assertTrue(comparator.compare(named("abc"), named("abc")) == 0);
    assertTrue(comparator.compare(named("def"), named("abc")) > 0);
    assertTrue(comparator.compare(named("def"), named("ghi")) < 0);
    assertTrue(comparator.compare(named("def"), named("def")) == 0);
    assertTrue(comparator.compare(named("ghi"), named("abc")) > 0);
    assertTrue(comparator.compare(named("abc"), named("ABC")) == 0);
    assertTrue(comparator.compare(named("ABC"), named("abc")) == 0);
  }
}
