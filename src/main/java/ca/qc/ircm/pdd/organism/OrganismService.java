/*
 * Copyright (c) 2006 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.pdd.organism;

import static ca.qc.ircm.pdd.organism.QOrganism.organism;

import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import java.util.Collection;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Services for {@link Organism}.
 */
@Service
@Transactional
public class OrganismService {
  @PersistenceContext
  private EntityManager entityManager;
  @Inject
  private JPAQueryFactory queryFactory;

  protected OrganismService() {
  }

  protected OrganismService(EntityManager entityManager, JPAQueryFactory queryFactory) {
    this.entityManager = entityManager;
    this.queryFactory = queryFactory;
  }

  /**
   * Returns organism with specified id.
   *
   * @param id
   *          id
   * @return organism with specified id
   */
  public Organism get(Integer id) {
    if (id == null) {
      return null;
    }

    return entityManager.find(Organism.class, id);
  }

  /**
   * Returns all organisms.
   *
   * @return all organisms
   */
  public List<Organism> all() {
    JPAQuery<Organism> query = queryFactory.select(organism);
    query.from(organism);
    return query.fetch();
  }

  /**
   * Returns true if database contains at least one organism, false otherwise.
   *
   * @return true if database contains at least one organism, false otherwise
   */
  public boolean containsAny() {
    JPAQuery<Organism> query = queryFactory.select(organism);
    query.from(organism);
    return query.fetchCount() > 0;
  }

  public void insert(Organism organism) {
    entityManager.persist(organism);
  }

  public void update(Organism organism) {
    entityManager.merge(organism);
  }

  public void delete(Collection<Organism> organisms) {
    organisms.forEach(o -> delete(o));
  }

  private void delete(Organism organism) {
    organism = entityManager.merge(organism);
    entityManager.refresh(organism);
    entityManager.remove(organism);
  }
}
