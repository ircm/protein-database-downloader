/*
 * Copyright (c) 2006 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.pdd.download.gui;

import ca.qc.ircm.javafx.message.MessageDialog;
import ca.qc.ircm.javafx.message.MessageDialog.MessageDialogType;
import ca.qc.ircm.pdd.download.DownloadParameters;
import ca.qc.ircm.pdd.download.DownloadParametersBean;
import ca.qc.ircm.pdd.download.DownloadProteinDatabaseTask;
import ca.qc.ircm.pdd.download.DownloadProteinDatabaseTaskFactory;
import ca.qc.ircm.pdd.download.ProteinDatabase;
import ca.qc.ircm.pdd.gui.ProgressDialog;
import ca.qc.ircm.pdd.organism.Organism;
import ca.qc.ircm.pdd.organism.gui.OrganismStringConverter;
import java.io.File;
import java.nio.file.Path;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.concurrent.Worker.State;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Window;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Gene finder controller.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class DownloadPresenter {
  private static final Logger logger = LoggerFactory.getLogger(DownloadPresenter.class);
  private ListProperty<Organism> organismsProperty = new SimpleListProperty<>();
  private ObjectProperty<ProteinDatabase> databaseProperty = new SimpleObjectProperty<>();
  private BooleanProperty prohitsProperty = new SimpleBooleanProperty();
  private BooleanProperty decoyProperty = new SimpleBooleanProperty();
  @FXML
  private ResourceBundle resources;
  @FXML
  private Pane view;
  @FXML
  private Label organismLabel;
  @FXML
  private ChoiceBox<Organism> organism;
  @FXML
  private Label databaseLabel;
  @FXML
  private ChoiceBox<ProteinDatabase> database;
  @FXML
  private CheckBox prohits;
  @FXML
  private CheckBox decoy;
  @Autowired
  private DownloadProteinDatabaseTaskFactory downloadProteinDatabaseTaskFactory;
  @Autowired
  private ExecutorService executorService;
  private FileChooser fileChooser = new FileChooser();
  private Locale locale = Locale.getDefault();

  @FXML
  private void initialize() {
    fileChooser.getExtensionFilters()
        .add(new ExtensionFilter(resources.getString("file.description"), "*.fasta"));

    organism.setItems(organismsProperty);
    organism.setConverter(new OrganismStringConverter());
    organismsProperty.addListener((ListChangeListener<Organism>) event -> {
      while (event.next()) {
        if (event.wasAdded() || event.wasRemoved()) {
          Platform.runLater(() -> {
            if (!organism.getItems().isEmpty()) {
              organism.getSelectionModel().select(0);
            }
          });
        }
      }
    });
    database.setItems(FXCollections.observableArrayList(ProteinDatabase.values()));
    database.setConverter(new ProteinDatabaseStringConverter(locale));
    database.valueProperty().bindBidirectional(databaseProperty);
    prohits.selectedProperty().bindBidirectional(prohitsProperty);
    decoy.selectedProperty().bindBidirectional(decoyProperty);

    // Default values.
    database.setValue(ProteinDatabase.REFSEQ);
  }

  public ListProperty<Organism> organismsProperty() {
    return organismsProperty;
  }

  private DownloadParameters getParameters() {
    DownloadParametersBean parameters = new DownloadParametersBean();
    parameters.organism(organism.getSelectionModel().getSelectedItem());
    parameters.proteinDatabase(databaseProperty.get());
    parameters.prohits(prohitsProperty.get());
    parameters.decoy(decoyProperty.get());
    return parameters;
  }

  @FXML
  private void download() {
    if (validate()) {
      File rawOutput = fileChooser.showSaveDialog(view.getScene().getWindow());
      if (rawOutput != null) {
        Path output = rawOutput.toPath();
        DownloadParameters parameters = getParameters();
        final Window window = this.view.getScene().getWindow();
        final DownloadProteinDatabaseTask task =
            downloadProteinDatabaseTaskFactory.create(parameters, output, Locale.getDefault());
        new ProgressDialog(window, task);
        task.stateProperty().addListener((observable, oldValue, newValue) -> {
          if (newValue == State.FAILED) {
            Throwable error = task.getException();
            logger.error("failed", error);
            showFailedMessage(error, output);
          } else if (newValue == State.SUCCEEDED) {
            showSuccessMessage(output, task.getValue());
          }
        });
        executorService.submit(task);
      }
    }
  }

  private boolean validate() {
    organismLabel.getStyleClass().remove("error");
    organism.getStyleClass().remove("error");
    List<String> errors = new ArrayList<String>();
    if (organism.getSelectionModel().getSelectedItem() == null) {
      errors.add(resources.getString("error.organism.required"));
      organismLabel.getStyleClass().add("error");
      organism.getStyleClass().add("error");
    }
    boolean valid = errors.isEmpty();
    if (!valid) {
      new MessageDialog(view.getScene().getWindow(), MessageDialogType.ERROR,
          resources.getString("error.title"), errors);
    }
    return valid;
  }

  private void showSuccessMessage(Path output, String message) {
    final Window window = this.view.getScene().getWindow();
    new MessageDialog(window, MessageDialogType.INFORMATION,
        resources.getString("task.succeeded.title"),
        MessageFormat.format(resources.getString("task.succeeded.message"), output), message);
  }

  private void showFailedMessage(Throwable error, Path output) {
    final Window window = this.view.getScene().getWindow();
    new MessageDialog(window, MessageDialogType.ERROR, resources.getString("task.failed.title"),
        MessageFormat.format(resources.getString("task.failed.message"), output.getFileName()),
        error.getMessage());
  }
}
