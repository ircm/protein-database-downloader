/*
 * Copyright (c) 2006 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.pdd.download;

import ca.qc.ircm.progressbar.ProgressBar;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Locale;

/**
 * Service to download files.
 */
public interface DownloadService {
  public static final String INTERRUPTED_MESSAGE = "Interrupted protein sequence fetching";

  /**
   * Download protein database for organism.
   *
   * @param parameters
   *          download parameters
   * @param output
   *          where to write protein database
   * @param progressBar
   *          records download progression
   * @param locale
   *          user's locale
   * @return message to show to user
   * @throws IOException
   *           could not connect to remote server or read files
   * @throws InterruptedException
   *           download was interrupted
   */
  public String downloadProteinDatabase(DownloadParameters parameters, Path output,
      ProgressBar progressBar, Locale locale) throws IOException, InterruptedException;
}
