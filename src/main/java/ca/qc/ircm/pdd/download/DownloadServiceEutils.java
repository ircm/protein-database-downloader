/*
 * Copyright (c) 2006 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.pdd.download;

import ca.qc.ircm.pdd.annotation.Ncbi;
import ca.qc.ircm.pdd.annotation.NcbiConfiguration;
import ca.qc.ircm.pdd.annotation.SequenceConfiguration;
import ca.qc.ircm.pdd.ftp.FtpService;
import ca.qc.ircm.pdd.organism.Organism;
import ca.qc.ircm.pdd.rest.RestClientFactory;
import ca.qc.ircm.pdd.util.ExceptionUtils;
import ca.qc.ircm.pdd.util.MessageResource;
import ca.qc.ircm.pdd.utils.xml.StackSaxHandler;
import ca.qc.ircm.progressbar.ProgressBar;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.glassfish.jersey.client.ClientProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

@Component
@Ncbi
public class DownloadServiceEutils extends DownloadServiceAdditionalSequences
    implements DownloadService {
  private static class SearchOutput {
    private String webEnv;
    private String queryKey;
    private Integer count;
  }

  private static class ProteinDatabaseVersion {
    private List<String> proteinIds;
    private String version;

    private ProteinDatabaseVersion(List<String> proteinIds) {
      this(proteinIds, null);
    }

    private ProteinDatabaseVersion(List<String> proteinIds, String version) {
      this.proteinIds = proteinIds;
      this.version = version;
    }
  }

  private static final Logger logger = LoggerFactory.getLogger(DownloadServiceEutils.class);
  private static final int MAX_RETRIES = 50;
  private static final long SLEEP_TIME = 10000;
  private static final Pattern SEQUENCE_ID_PATTERN = Pattern.compile("\\w{2,3}\\|([\\w\\.]+)");

  @Inject
  private NcbiConfiguration ncbiConfiguration;
  @Inject
  private RestClientFactory restClientFactory;
  @Inject
  private FtpService ftpService;

  protected DownloadServiceEutils() {
  }

  protected DownloadServiceEutils(SequenceConfiguration sequenceConfiguration,
      NcbiConfiguration ncbiConfiguration, RestClientFactory restClientFactory,
      FtpService ftpService) {
    super(sequenceConfiguration);
    this.ncbiConfiguration = ncbiConfiguration;
    this.restClientFactory = restClientFactory;
    this.ftpService = ftpService;
  }

  private List<String> validateOutputFile(Path output, List<String> ids, MessageResource resources)
      throws IOException {
    List<String> invalidIds = new ArrayList<>();
    Set<String> alreadyDownloadedIds = parseOutputFileIds(output);
    Set<String> idsAsSet = new HashSet<>(ids);
    alreadyDownloadedIds.forEach(id -> {
      if (!idsAsSet.contains(id)) {
        invalidIds.add(id);
      }
    });
    return invalidIds;
  }

  private Set<String> idsFromLine(String line) {
    Set<String> ids = new HashSet<>();
    if (line.startsWith(">")) {
      Matcher matcher = SEQUENCE_ID_PATTERN.matcher(line);
      while (matcher.find()) {
        ids.add(matcher.group(1));
      }
      if (!matcher.find(0)) {
        ids.add(line);
      }
    }
    return ids;
  }

  private Set<String> parseOutputFileIds(Path output) throws IOException {
    Set<String> gis = new HashSet<>();
    try (BufferedReader reader = Files.newBufferedReader(output)) {
      String line;
      while ((line = reader.readLine()) != null) {
        if (line.startsWith(">")) {
          gis.addAll(idsFromLine(line));
        }
      }
    }
    return gis;
  }

  private void removeIdsFromFile(Set<String> ids, Path output) throws IOException {
    if (ids.isEmpty()) {
      return;
    }

    Path temp = Files.createTempFile("protein_database", "fasta");
    try (BufferedReader reader = Files.newBufferedReader(output, FASTA_CHARSET);
        BufferedWriter writer =
            Files.newBufferedWriter(temp, FASTA_CHARSET, StandardOpenOption.CREATE)) {
      String line;
      boolean copySequence = true;
      while ((line = reader.readLine()) != null) {
        if (line.startsWith(">")) {
          Set<String> lineIds = idsFromLine(line);
          copySequence = !lineIds.isEmpty() && Collections.disjoint(ids, lineIds);
          if (copySequence) {
            writer.write(line);
            writer.write("\n");
          }
        } else if (copySequence) {
          writer.write(line);
          writer.write("\n");
        }
      }
    }
    Files.move(temp, output, StandardCopyOption.REPLACE_EXISTING);
  }

  @Override
  public String downloadProteinDatabase(DownloadParameters parameters, Path output,
      ProgressBar progressBar, Locale locale) throws IOException, InterruptedException {
    if (parameters.getProteinDatabase() != ProteinDatabase.REFSEQ
        && parameters.getProteinDatabase() != ProteinDatabase.NCBI) {
      throw new IllegalArgumentException(
          "proteinDatabase " + parameters.getProteinDatabase() + " not supported, only "
              + ProteinDatabase.REFSEQ + " and " + ProteinDatabase.NCBI + " is supported");
    }
    MessageResource resources = new MessageResource(DownloadService.class, locale);
    ExceptionUtils.throwIfInterrupted(INTERRUPTED_MESSAGE);
    ProteinDatabaseVersion proteins = getIds(parameters, progressBar.step(0.08), locale);
    ExceptionUtils.throwIfInterrupted(INTERRUPTED_MESSAGE);
    if (parameters.getProteinDatabase() == ProteinDatabase.REFSEQ) {
      proteins.version = String.valueOf(releaseNumber(progressBar.step(0.1), resources, locale));
      ExceptionUtils.throwIfInterrupted(INTERRUPTED_MESSAGE);
    }
    progressBar.setProgress(0.1);
    final int totalCount = proteins.proteinIds.size();
    ExceptionUtils.throwIfInterrupted(INTERRUPTED_MESSAGE);
    if (Files.exists(output)) {
      if (validFasta(output)) {
        ExceptionUtils.throwIfInterrupted(INTERRUPTED_MESSAGE);
        progressBar.setMessage(resources.message("sequence.skipping", output));
        Set<String> alreadyDownloadedIds = new HashSet<>(parseOutputFileIds(output));
        Set<String> idsToRemove =
            new HashSet<>(validateOutputFile(output, proteins.proteinIds, resources));
        if (!idsToRemove.isEmpty()) {
          progressBar.setMessage(resources.message("error.additional.sequences", output));
          removeIdsFromFile(idsToRemove, output);
        }
        proteins.proteinIds = proteins.proteinIds.stream()
            .filter(id -> !alreadyDownloadedIds.contains(id)).collect(Collectors.toList());
      } else {
        ExceptionUtils.throwIfInterrupted(INTERRUPTED_MESSAGE);
        progressBar.setMessage(resources.message("error.not.fasta", output));
        Files.delete(output);
      }
    }
    progressBar.setProgress(0.15);
    ExceptionUtils.throwIfInterrupted(INTERRUPTED_MESSAGE);
    download(proteins.proteinIds, totalCount, output, progressBar.step(0.75), resources);
    ExceptionUtils.throwIfInterrupted(INTERRUPTED_MESSAGE);
    final String confirmMessage =
        countSequences(output, parameters.getProteinDatabase(), proteins, progressBar, resources);
    progressBar.setProgress(0.95);
    ExceptionUtils.throwIfInterrupted(INTERRUPTED_MESSAGE);
    if (parameters.isProhits()) {
      progressBar.setMessage(resources.message("prohits", output));
      addProhitsSequences(output);
    }
    ExceptionUtils.throwIfInterrupted(INTERRUPTED_MESSAGE);
    if (parameters.isDecoy()) {
      progressBar.setMessage(resources.message("decoy", output));
      addDecoySequences(output);
    }
    progressBar.setProgress(1.0);
    return confirmMessage;
  }

  private ProteinDatabaseVersion getIds(DownloadParameters parameters, ProgressBar progressBar,
      Locale locale) throws IOException, InterruptedException {
    MessageResource resources = new MessageResource(DownloadService.class, locale);
    ExceptionUtils.throwIfInterrupted(INTERRUPTED_MESSAGE);
    SearchOutput searchOutput = search(parameters.getOrganism(), parameters.getProteinDatabase(),
        progressBar.step(0.05), resources);
    ExceptionUtils.throwIfInterrupted(INTERRUPTED_MESSAGE);
    List<String> ids = getIds(searchOutput, progressBar.step(0.05), resources);
    return new ProteinDatabaseVersion(ids);
  }

  private List<String> getIds(SearchOutput searchOutput, ProgressBar progressBar,
      MessageResource resources) throws IOException, InterruptedException {
    final int maxIdsPerRequest = ncbiConfiguration.getMaxIdsPerRequest();
    Client client = restClientFactory.createClient();
    client.property(ClientProperties.CONNECT_TIMEOUT, REST_TIMEOUT);
    client.property(ClientProperties.READ_TIMEOUT, REST_TIMEOUT);
    WebTarget target = client.target(ncbiConfiguration.getEutils());
    target = target.path("efetch.fcgi");
    target = target.queryParam("db", "protein");
    target = target.queryParam("WebEnv", searchOutput.webEnv);
    target = target.queryParam("query_key", searchOutput.queryKey);
    target = target.queryParam("rettype", "acc");
    target = target.queryParam("retmax", maxIdsPerRequest);
    Set<String> ids = new LinkedHashSet<>();
    for (int i = 0; i < searchOutput.count; i += maxIdsPerRequest) {
      progressBar.setMessage(resources.message("getIds", i + 1,
          Math.min(i + maxIdsPerRequest, searchOutput.count), searchOutput.count));
      ExceptionUtils.throwIfInterrupted(INTERRUPTED_MESSAGE);
      WebTarget stepTarget = target.queryParam("retstart", i);
      try (BufferedReader reader = new BufferedReader(
          new InputStreamReader(stepTarget.request().get(InputStream.class), FASTA_CHARSET))) {
        ExceptionUtils.throwIfInterrupted(INTERRUPTED_MESSAGE);
        progressBar.setProgress(0.75);
        String line;
        while ((line = reader.readLine()) != null) {
          if (!line.isEmpty() && !ids.add(line)) {
            logger.warn("id {} already parsed before", line);
          }
        }
        progressBar.setProgress(1.0);
      }
    }
    return new ArrayList<>(ids);
  }

  private SearchOutput search(Organism organism, ProteinDatabase proteinDatabase,
      ProgressBar progressBar, MessageResource resources) throws IOException, InterruptedException {
    progressBar.setMessage(resources.message("search"));
    Client client = restClientFactory.createClient();
    client.property(ClientProperties.CONNECT_TIMEOUT, REST_TIMEOUT);
    WebTarget target = client.target(ncbiConfiguration.getEutils());
    target = target.path("esearch.fcgi");
    target = target.queryParam("db", "protein");
    if (proteinDatabase == ProteinDatabase.REFSEQ) {
      target =
          target.queryParam("term", "txid" + organism.getId() + "[Organism] AND refseq[filter]");
    } else {
      target = target.queryParam("term", "txid" + organism.getId() + "[Organism]");
    }
    target = target.queryParam("usehistory", "y");
    try (InputStream searchInput = target.request().get(InputStream.class)) {
      ExceptionUtils.throwIfInterrupted(INTERRUPTED_MESSAGE);
      progressBar.setProgress(0.75);
      SearchOutput searchOutput = parseSearchOutput(searchInput);
      progressBar.setProgress(1.0);
      return searchOutput;
    } catch (ParserConfigurationException | SAXException e) {
      throw new IOException("Could not read NCBI search content", e);
    }
  }

  private SearchOutput parseSearchOutput(InputStream input)
      throws IOException, ParserConfigurationException, SAXException {
    SAXParserFactory factory = SAXParserFactory.newInstance();
    SAXParser parser = factory.newSAXParser();
    SearchOutput seachOutput = new SearchOutput();
    parser.parse(input, new StackSaxHandler() {
      private StringBuilder builder = new StringBuilder();

      @Override
      protected void startElement(String elementName, Attributes attributes) throws SAXException {
        super.startElement(elementName, attributes);

        if (current("Count") && parent("eSearchResult")) {
          builder.setLength(0);
        } else if (current("QueryKey")) {
          builder.setLength(0);
        } else if (current("WebEnv")) {
          builder.setLength(0);
        }
      }

      @Override
      protected void endElement(String elementName) {
        super.endElement(elementName);

        if (current("Count") && parent("eSearchResult")) {
          seachOutput.count = Integer.parseInt(builder.toString());
        } else if (current("QueryKey")) {
          seachOutput.queryKey = builder.toString();
        } else if (current("WebEnv")) {
          seachOutput.webEnv = builder.toString();
        }
      }

      @Override
      public void characters(char[] ch, int start, int length) throws SAXException {
        if (builder != null) {
          builder.append(ch, start, length);
        }
      }
    });
    return seachOutput;
  }

  private FTPClient client(ProgressBar progressBar, MessageResource resources) throws IOException {
    String ftpServer = ncbiConfiguration.getFtp();
    progressBar.setMessage(resources.message("ftp.connect", ftpServer));
    FTPClient client = ftpService.anonymousConnect(ftpServer);
    progressBar.setProgress(1.0);
    return client;
  }

  private int releaseNumber(ProgressBar progressBar, MessageResource resources, Locale locale)
      throws IOException {
    progressBar.setMessage(resources.message("releaseNumber"));
    String releaseNumberFilename = ncbiConfiguration.getRefseqReleaseNumber();
    logger.debug("get release number from {}", releaseNumberFilename);
    Path releaseNumberFile = ftpService.localFile(releaseNumberFilename);
    FTPClient client = client(progressBar.step(0.1), resources);
    try {
      ftpService.downloadFile(client, releaseNumberFilename, releaseNumberFile, progressBar,
          locale);
    } finally {
      if (client.isConnected()) {
        client.disconnect();
      }
    }
    int releaseNumber;
    try (BufferedReader reader = Files.newBufferedReader(releaseNumberFile, FASTA_CHARSET)) {
      String rawReleaseNumber = reader.readLine();
      try {
        releaseNumber = Integer.parseInt(rawReleaseNumber);
      } catch (NumberFormatException e) {
        throw new IOException("No relase number not found in release number file");
      }
    }
    progressBar.setProgress(1.0);
    return releaseNumber;
  }

  private void download(List<String> ids, int totalCount, Path output, ProgressBar progressBar,
      MessageResource resources) throws IOException, InterruptedException {
    int maxIdsPerRequest = ncbiConfiguration.getMaxIdsPerRequest();
    int virtualStart = totalCount - ids.size();
    int steps = (int) Math.ceil((double) totalCount / maxIdsPerRequest);
    double progressStep = 1.0 / Math.max(steps, 1);
    int currentStep = (totalCount - ids.size()) / maxIdsPerRequest;
    progressBar.setProgress(progressStep * currentStep);
    int retries = 0;
    for (int i = 0; i < ids.size(); i += maxIdsPerRequest) {
      ExceptionUtils.throwIfInterrupted(INTERRUPTED_MESSAGE);
      progressBar.setMessage(resources.message("download.partial", virtualStart + i + 1,
          Math.min(virtualStart + i + maxIdsPerRequest, totalCount), totalCount));
      boolean succes = downloadSequenceArray(ids, i, output);
      if (succes) {
        retries = 0;
        progressBar.setProgress(progressStep * ++currentStep);
      } else {
        progressBar.setMessage(
            resources.message("download.partial.failed", i + 1, i + maxIdsPerRequest, ids.size()));
        logger.warn("EUtils did not return a fasta file for sequences {} to {}", i,
            i + maxIdsPerRequest);
        retries++;
        if (retries > MAX_RETRIES) {
          throw new IOException(
              "Reach maximum number of retries in sequences " + (currentStep + 1) + " of " + steps);
        } else {
          Thread.sleep(SLEEP_TIME);
          i -= maxIdsPerRequest;
        }
      }
    }
    progressBar.setProgress(1.0);
  }

  private boolean downloadSequenceArray(List<String> ids, int start, Path output)
      throws IOException, InterruptedException {
    final List<String> fetchGis =
        ids.subList(start, Math.min(start + ncbiConfiguration.getMaxIdsPerRequest(), ids.size()));
    Client client = restClientFactory.createClient();
    client.property(ClientProperties.CONNECT_TIMEOUT, REST_TIMEOUT);
    client.property(ClientProperties.READ_TIMEOUT, REST_TIMEOUT);
    WebTarget target = client.target(ncbiConfiguration.getEutils());
    target = target.path("efetch.fcgi");

    Form form = new Form();
    form.param("db", "protein");
    fetchGis.forEach(id -> {
      form.param("id", id);
    });
    form.param("rettype", "fasta");
    try (
        InputStream input = new BufferedInputStream(target.request().post(
            Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE), InputStream.class));
        ByteArrayOutputStream testOutput = new ByteArrayOutputStream()) {
      IOUtils.copy(input, testOutput);
      byte[] rawInput = testOutput.toByteArray();
      String restIntput = new String(rawInput, FASTA_CHARSET);
      if (validFasta(new BufferedReader(new StringReader(restIntput)))
          && validSequences(restIntput, fetchGis)) {
        Files.write(output, rawInput, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        return true;
      } else {
        return false;
      }
    }
  }

  private boolean validFasta(Path file) throws IOException {
    try (BufferedReader reader = Files.newBufferedReader(file, FASTA_CHARSET)) {
      return validFasta(reader);
    }
  }

  private boolean validFasta(BufferedReader reader) throws IOException {
    boolean valid = true;
    String line;
    while ((line = reader.readLine()) != null) {
      if (!line.isEmpty() && !line.startsWith(">")) {
        if (!StringUtils.isAlpha(line)) {
          valid = false;
          logger.warn("Line {} is not compatible with fasta format", line);
        }
      }
    }
    return valid;
  }

  private boolean validSequences(String input, List<String> expectedIds)
      throws IOException, InterruptedException {
    Pattern onlyWordCharactersPattern = Pattern.compile("([a-zA-Z0-9]+).*");
    Map<String, Boolean> expectedIdsFound = new HashMap<>();
    expectedIds.stream().filter(id -> onlyWordCharactersPattern.matcher(id).matches()).map(id -> {
      Matcher matcher = onlyWordCharactersPattern.matcher(id);
      matcher.matches();
      return matcher.group(1);
    }).forEach(idPattern -> expectedIdsFound.put(idPattern, false));
    Map<String, Pattern> expectedIdsPatterns = new HashMap<>();
    expectedIdsFound.keySet()
        .forEach(p -> expectedIdsPatterns.put(p, Pattern.compile("[\\|\\>]" + p)));
    String[] lines = input.split("\\r?\\n", -1);
    boolean valid = true;
    for (int i = 0; valid && i < lines.length; i++) {
      String line = lines[i];
      if (line.startsWith(">")) {
        Optional<String> foundIdPattern = expectedIdsPatterns.entrySet().stream()
            .filter(e -> e.getValue().matcher(line).find()).map(e -> e.getKey()).findAny();
        if (!foundIdPattern.isPresent()) {
          valid = false;
          logger.warn("No expected sequences found in sequence name {}", line);
        } else {
          expectedIdsFound.put(foundIdPattern.get(), true);
        }
      }
    }
    Optional<String> missing = expectedIdsFound.entrySet().stream().filter(e -> !e.getValue())
        .map(e -> e.getKey()).findAny();
    if (missing.isPresent()) {
      valid = false;
      logger.warn("Sequence id {} is missing", missing.get());
    }
    try (BufferedReader reader = new BufferedReader(new StringReader(input))) {
      long count = countSequences(reader);
      if (count != expectedIds.size()) {
        valid = false;
        logger.warn("Expected {} sequences, found {}", expectedIds.size(), count);
      }
    }
    return valid;
  }

  protected String countSequences(Path output, ProteinDatabase proteinDatabase,
      ProteinDatabaseVersion proteins, ProgressBar progressBar, MessageResource resources)
      throws IOException, InterruptedException {
    long count = countSequences(output);
    progressBar.setProgress(1.0);
    if (proteinDatabase == ProteinDatabase.REFSEQ) {
      return resources.message("refseq.count", count, proteins.version);
    } else {
      return resources.message("count", count);
    }
  }
}
