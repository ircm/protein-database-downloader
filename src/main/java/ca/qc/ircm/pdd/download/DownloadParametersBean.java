/*
 * Copyright (c) 2006 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.pdd.download;

import ca.qc.ircm.pdd.organism.Organism;

public class DownloadParametersBean implements DownloadParameters {
  private Organism organism;
  private ProteinDatabase proteinDatabase;
  private boolean prohits;
  private boolean decoy;

  @Override
  public Organism getOrganism() {
    return organism;
  }

  public DownloadParametersBean organism(Organism organism) {
    this.organism = organism;
    return this;
  }

  @Override
  public ProteinDatabase getProteinDatabase() {
    return proteinDatabase;
  }

  public DownloadParametersBean proteinDatabase(ProteinDatabase proteinDatabase) {
    this.proteinDatabase = proteinDatabase;
    return this;
  }

  @Override
  public boolean isProhits() {
    return prohits;
  }

  public DownloadParametersBean prohits(boolean prohits) {
    this.prohits = prohits;
    return this;
  }

  @Override
  public boolean isDecoy() {
    return decoy;
  }

  public DownloadParametersBean decoy(boolean decoy) {
    this.decoy = decoy;
    return this;
  }
}
