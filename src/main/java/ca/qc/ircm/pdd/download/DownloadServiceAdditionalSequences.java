/*
 * Copyright (c) 2006 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.pdd.download;

import ca.qc.ircm.pdd.annotation.SequenceConfiguration;
import ca.qc.ircm.pdd.util.ExceptionUtils;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;
import javax.inject.Inject;
import org.apache.commons.io.IOUtils;

public abstract class DownloadServiceAdditionalSequences implements DownloadService {
  protected static final Charset FASTA_CHARSET = Charset.forName("UTF-8");
  protected static final int REST_TIMEOUT = 120000;
  private static final int FASTA_SEQUENCE_MAX_LINE_LENGTH = 70;
  @Inject
  private SequenceConfiguration sequenceConfiguration;

  protected DownloadServiceAdditionalSequences() {
  }

  protected DownloadServiceAdditionalSequences(SequenceConfiguration sequenceConfiguration) {
    this.sequenceConfiguration = sequenceConfiguration;
  }

  protected void addProhitsSequences(Path output) throws IOException {
    String filename = "prohits_additions.fasta";
    try (
        InputStream input = new BufferedInputStream(getClass().getResourceAsStream("/" + filename));
        BufferedOutputStream outputStream =
            new BufferedOutputStream(Files.newOutputStream(output, StandardOpenOption.APPEND))) {
      IOUtils.copy(input, outputStream);
    }
  }

  protected void addDecoySequences(Path output) throws IOException, InterruptedException {
    Random random = new Random();
    int[] aas = new int[26];
    int index = 0;
    for (int aa = 'A'; aa <= 'Z'; aa++) {
      aas[index++] = count(aa, output);
    }
    int sum = IntStream.of(aas).sum();
    double[] aaProbabilities = new double[aas.length];
    for (index = 0; index < aas.length; index++) {
      aaProbabilities[index] = (double) aas[index] / sum;
    }
    List<Integer> lengths = sequenceLengths(output);
    try (BufferedWriter writer = Files.newBufferedWriter(output, StandardOpenOption.APPEND)) {
      String sequenceName = sequenceConfiguration.getDecoyName();
      int decoyIndex = 1;
      for (int i = 0; i < lengths.size(); i++) {
        ExceptionUtils.throwIfInterrupted(INTERRUPTED_MESSAGE);
        int length = lengths.get(i);
        String sequence = generateSequence(random, aaProbabilities, length);
        writeFasta(MessageFormat.format(sequenceName, decoyIndex++), sequence, writer);
      }
    }
  }

  private int count(int aa, Path fasta) throws IOException, InterruptedException {
    int count = 0;
    try (BufferedReader reader = Files.newBufferedReader(fasta)) {
      String line;
      while ((line = reader.readLine()) != null) {
        ExceptionUtils.throwIfInterrupted(INTERRUPTED_MESSAGE);
        if (line.startsWith(";")) {
          continue;
        } else if (line.startsWith(">")) {
          continue;
        } else {
          count += line.toUpperCase().chars().filter(l -> l == aa).count();
        }
      }
    }
    return count;
  }

  private List<Integer> sequenceLengths(Path fasta) throws IOException, InterruptedException {
    List<Integer> lengths = new ArrayList<>();
    try (BufferedReader reader = Files.newBufferedReader(fasta)) {
      int length = 0;
      String line;
      while ((line = reader.readLine()) != null) {
        ExceptionUtils.throwIfInterrupted(INTERRUPTED_MESSAGE);
        if (line.startsWith(";")) {
          continue;
        } else if (line.startsWith(">")) {
          if (length > 0) {
            lengths.add(length);
          }
          length = 0;
        } else {
          length += line.trim().length();
        }
      }
    }
    return lengths;
  }

  private String generateSequence(Random random, double[] aaProbabilities, int length) {
    return IntStream.range(0, length).map(i -> indexOf(random.nextDouble(), aaProbabilities) + 'A')
        .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
        .toString();
  }

  private int indexOf(double randomValue, double[] aaProbabilities) {
    int index = 0;
    while (randomValue > 0 && index < aaProbabilities.length) {
      randomValue -= aaProbabilities[index++];
    }
    return index - 1;
  }

  private void writeFasta(String sequenceName, String sequence, Writer writer) throws IOException {
    writer.write(">" + sequenceName + "\n");
    int index = 0;
    for (char aa : sequence.toCharArray()) {
      if (index++ == FASTA_SEQUENCE_MAX_LINE_LENGTH) {
        writer.write("\n");
        index = 1;
      }
      writer.write(aa);
    }
    writer.write("\n");
  }

  protected long countSequences(Path output) throws IOException, InterruptedException {
    try (BufferedReader reader = Files.newBufferedReader(output, FASTA_CHARSET)) {
      return countSequences(reader);
    }
  }

  protected long countSequences(BufferedReader reader) throws IOException, InterruptedException {
    long count = 0;
    String line;
    while ((line = reader.readLine()) != null) {
      ExceptionUtils.throwIfInterrupted(INTERRUPTED_MESSAGE);
      if (line.startsWith(">")) {
        count++;
      }
    }
    return count;
  }
}
