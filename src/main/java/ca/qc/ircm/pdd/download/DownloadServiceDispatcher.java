/*
 * Copyright (c) 2006 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.pdd.download;

import ca.qc.ircm.pdd.annotation.Ncbi;
import ca.qc.ircm.pdd.annotation.Uniprot;
import ca.qc.ircm.progressbar.ProgressBar;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Locale;
import javax.inject.Inject;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Primary
public class DownloadServiceDispatcher implements DownloadService {
  @Inject
  @Ncbi
  private DownloadService ncbiDownloadService;
  @Inject
  @Uniprot
  private DownloadService uniprotDownloadService;

  protected DownloadServiceDispatcher() {
  }

  protected DownloadServiceDispatcher(DownloadService ncbiDownloadService,
      DownloadService uniprotDownloadService) {
    this.ncbiDownloadService = ncbiDownloadService;
    this.uniprotDownloadService = uniprotDownloadService;
  }

  @Override
  public String downloadProteinDatabase(DownloadParameters parameters, Path output,
      ProgressBar progressBar, Locale locale) throws IOException, InterruptedException {
    if (parameters.getProteinDatabase() == null) {
      throw new NullPointerException("proteinDatabase parameter cannot be null");
    }

    DownloadService downloadService = null;
    switch (parameters.getProteinDatabase()) {
      case NCBI:
        downloadService = ncbiDownloadService;
        break;
      case REFSEQ:
        downloadService = ncbiDownloadService;
        break;
      case UNIPROT:
        downloadService = uniprotDownloadService;
        break;
      case SWISSPROT:
        downloadService = uniprotDownloadService;
        break;
      default:
        throw new AssertionError(ProteinDatabase.class.getName() + " "
            + parameters.getProteinDatabase() + " not covered in switch");
    }
    return downloadService.downloadProteinDatabase(parameters, output, progressBar, locale);
  }
}
