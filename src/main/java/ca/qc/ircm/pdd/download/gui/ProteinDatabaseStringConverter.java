/*
 * Copyright (c) 2006 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.pdd.download.gui;

import ca.qc.ircm.pdd.download.ProteinDatabase;
import java.util.Locale;
import javafx.util.StringConverter;

/**
 * StringConverter for {@link ProteinDatabase}.
 */
public class ProteinDatabaseStringConverter extends StringConverter<ProteinDatabase> {
  private Locale locale;

  public ProteinDatabaseStringConverter(Locale locale) {
    this.locale = locale;
  }

  @Override
  public ProteinDatabase fromString(String value) {
    for (ProteinDatabase type : ProteinDatabase.values()) {
      if (type.getLabel(locale).equals(value)) {
        return type;
      }
    }
    return null;
  }

  @Override
  public String toString(ProteinDatabase type) {
    return type != null ? type.getLabel(locale) : "";
  }
}
