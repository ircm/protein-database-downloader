/*
 * Copyright (c) 2006 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.pdd.download;

import ca.qc.ircm.progressbar.JavafxProgressBar;
import java.nio.file.Path;
import java.util.Locale;
import javafx.concurrent.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Task that download protein database.
 */
public class DownloadProteinDatabaseTask extends Task<String> {
  private static final Logger logger = LoggerFactory.getLogger(DownloadProteinDatabaseTask.class);
  private DownloadParameters parameters;
  private Path output;
  private Locale locale;
  private DownloadService downloadService;

  /**
   * Creates download protein database task.
   *
   * @param parameters
   *          parameters
   * @param output
   *          output
   * @param locale
   *          locale
   * @param downloadService
   *          download service
   */
  public DownloadProteinDatabaseTask(DownloadParameters parameters, Path output, Locale locale,
      DownloadService downloadService) {
    this.parameters = parameters;
    this.output = output;
    this.locale = locale;
    this.downloadService = downloadService;
  }

  @Override
  protected String call() throws Exception {
    JavafxProgressBar progressBar = new JavafxProgressBar();
    progressBar.progress().addListener((observable, old,
        newValue) -> updateProgress(newValue.doubleValue(), Math.max(newValue.doubleValue(), 1.0)));
    progressBar.message().addListener((observable, old, newValue) -> {
      updateMessage(newValue);
      logger.trace("updateMessage {}", newValue);
    });
    String message =
        downloadService.downloadProteinDatabase(parameters, output, progressBar, locale);
    logger.debug("completed protein database download to {}", output);
    return message;
  }
}
