/*
 * Copyright (c) 2006 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.pdd.download;

import ca.qc.ircm.pdd.annotation.SequenceConfiguration;
import ca.qc.ircm.pdd.annotation.Uniprot;
import ca.qc.ircm.pdd.annotation.UniprotConfiguration;
import ca.qc.ircm.pdd.ftp.FtpService;
import ca.qc.ircm.pdd.taxonomy.DownloadTaxonomyService;
import ca.qc.ircm.pdd.util.ExceptionUtils;
import ca.qc.ircm.pdd.util.MessageResource;
import ca.qc.ircm.progressbar.ProgressBar;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import javax.inject.Inject;
import org.apache.commons.net.ftp.FTPClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@Uniprot
public class DownloadServiceUniprot extends DownloadServiceAdditionalSequences
    implements DownloadService {
  private static final Logger logger = LoggerFactory.getLogger(DownloadServiceUniprot.class);
  @Inject
  private DownloadTaxonomyService downloadTaxonomyService;
  @Inject
  private UniprotConfiguration uniprotConfiguration;
  @Inject
  private FtpService ftpService;

  protected DownloadServiceUniprot() {
  }

  protected DownloadServiceUniprot(SequenceConfiguration sequenceConfiguration,
      DownloadTaxonomyService downloadTaxonomyService, UniprotConfiguration uniprotConfiguration,
      FtpService ftpService) {
    super(sequenceConfiguration);
    this.downloadTaxonomyService = downloadTaxonomyService;
    this.uniprotConfiguration = uniprotConfiguration;
    this.ftpService = ftpService;
  }

  @Override
  public String downloadProteinDatabase(DownloadParameters parameters, Path output,
      ProgressBar progressBar, Locale locale) throws IOException, InterruptedException {
    if (parameters.getProteinDatabase() != ProteinDatabase.UNIPROT
        && parameters.getProteinDatabase() != ProteinDatabase.SWISSPROT) {
      throw new IllegalArgumentException(
          "proteinDatabase " + parameters.getProteinDatabase() + " not supported, only "
              + ProteinDatabase.UNIPROT + " and " + ProteinDatabase.SWISSPROT + " is supported");
    }

    MessageResource resources = new MessageResource(DownloadService.class, locale);
    Set<Integer> includeOrganisms = downloadTaxonomyService
        .children(parameters.getOrganism().getId(), progressBar.step(0.05), locale);
    includeOrganisms.add(parameters.getOrganism().getId());
    ExceptionUtils.throwIfInterrupted(INTERRUPTED_MESSAGE);
    String idmappingFilename = uniprotConfiguration.getIdmapping();
    progressBar.setMessage(resources.message("uniprot.download.idmapping"));
    final Path idmapping = download(idmappingFilename, progressBar.step(0.2), locale);
    progressBar.setProgress(0.2);
    ExceptionUtils.throwIfInterrupted(INTERRUPTED_MESSAGE);
    String swissprotFastaFilename = uniprotConfiguration.getSwissprotFasta();
    progressBar.setMessage(resources.message("uniprot.download.swissprotFasta"));
    final Path swissprotFasta = download(swissprotFastaFilename, progressBar.step(0.1), locale);
    progressBar.setProgress(parameters.getProteinDatabase() == ProteinDatabase.UNIPROT ? 0.3 : 0.5);
    Path tremblFasta = null;
    if (parameters.getProteinDatabase() == ProteinDatabase.UNIPROT) {
      ExceptionUtils.throwIfInterrupted(INTERRUPTED_MESSAGE);
      String tremblFastaFilename = uniprotConfiguration.getTremblFasta();
      progressBar.setMessage(resources.message("uniprot.download.tremblFasta"));
      tremblFasta = download(tremblFastaFilename, progressBar.step(0.2), locale);
      progressBar.setProgress(0.5);
    }
    ExceptionUtils.throwIfInterrupted(INTERRUPTED_MESSAGE);
    progressBar.setMessage(resources.message("uniprot.parse.idmapping"));
    final Set<String> ids = new HashSet<>(
        organismIdsFromIdMapping(idmapping, includeOrganisms, resources, progressBar));
    progressBar.setProgress(0.7);
    ExceptionUtils.throwIfInterrupted(INTERRUPTED_MESSAGE);
    progressBar.setMessage(resources.message("uniprot.parse.swissprotFasta"));
    Files.write(output, new byte[0]);
    copyFastaSequences(swissprotFasta, output, ids, resources, progressBar);
    progressBar
        .setProgress(parameters.getProteinDatabase() == ProteinDatabase.UNIPROT ? 0.75 : 0.9);
    if (parameters.getProteinDatabase() == ProteinDatabase.UNIPROT) {
      ExceptionUtils.throwIfInterrupted(INTERRUPTED_MESSAGE);
      progressBar.setMessage(resources.message("uniprot.parse.tremblFasta"));
      copyFastaSequences(tremblFasta, output, ids, resources, progressBar);
      progressBar.setProgress(0.9);
    }
    ExceptionUtils.throwIfInterrupted(INTERRUPTED_MESSAGE);
    final long count = countSequences(output);
    progressBar.setProgress(0.95);
    ExceptionUtils.throwIfInterrupted(INTERRUPTED_MESSAGE);
    if (parameters.isProhits()) {
      progressBar.setMessage(resources.message("prohits", output));
      addProhitsSequences(output);
    }
    ExceptionUtils.throwIfInterrupted(INTERRUPTED_MESSAGE);
    if (parameters.isDecoy()) {
      progressBar.setMessage(resources.message("decoy", output));
      addDecoySequences(output);
    }
    progressBar.setProgress(1.0);
    return resources.message("count", count);
  }

  private Path download(String filename, ProgressBar progressBar, Locale locale)
      throws IOException {
    logger.debug("Download file {} from UniProt FTP", filename);
    FTPClient client = ftpService.anonymousConnect(uniprotConfiguration.getFtp());
    try {
      Path file = ftpService.localFile(filename);
      ftpService.downloadFile(client, filename, file, progressBar, locale);
      return file;
    } finally {
      if (client.isConnected()) {
        client.disconnect();
      }
    }
  }

  private List<String> organismIdsFromIdMapping(Path idmapping, Set<Integer> includeOrganisms,
      MessageResource resources, ProgressBar progressBar) throws IOException {
    progressBar.setMessage(resources.message("uniprot.parse.idmapping"));
    String taxonomyMappingName = uniprotConfiguration.getTaxonomyMappingName();
    List<String> ids = new ArrayList<>();
    try (BufferedReader reader = new BufferedReader(new InputStreamReader(
        new GZIPInputStream(Files.newInputStream(idmapping)), FASTA_CHARSET))) {
      String line;
      while ((line = reader.readLine()) != null) {
        String[] columns = line.split("\t", -1);
        if (columns[1].equals(taxonomyMappingName)
            && includeOrganisms.contains(Integer.valueOf(columns[2]))) {
          ids.add(columns[0]);
        }
      }
    }
    return ids;
  }

  private void copyFastaSequences(Path input, Path output, Set<String> includeIds,
      MessageResource resources, ProgressBar progressBar) throws IOException {
    try (
        BufferedReader reader = new BufferedReader(
            new InputStreamReader(new GZIPInputStream(Files.newInputStream(input)), FASTA_CHARSET));
        BufferedWriter writer =
            Files.newBufferedWriter(output, StandardOpenOption.CREATE, StandardOpenOption.APPEND)) {
      String line;
      Pattern sequenceNamePattern = Pattern.compile(">\\w{2}\\|(\\w+).*");
      boolean copySequence = false;
      while ((line = reader.readLine()) != null) {
        if (line.startsWith(">")) {
          copySequence = false;
          Matcher matcher = sequenceNamePattern.matcher(line);
          if (matcher.matches()) {
            String id = matcher.group(1);
            if (includeIds.contains(id)) {
              writer.write(line);
              writer.write("\n");
              copySequence = true;
            }
          }
        } else if (copySequence) {
          writer.write(line);
          writer.write("\n");
        }
      }
    }
  }
}
