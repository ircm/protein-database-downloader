/*
 * Copyright (c) 2006 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.pdd.annotation;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = UniprotConfigurationSpringBoot.PREFIX)
public class UniprotConfigurationSpringBoot implements UniprotConfiguration {
  public static final String PREFIX = "uniprot";
  private String ftp;
  private String swissprotFasta;
  private String tremblFasta;
  private String idmapping;
  private String taxonomyMappingName;

  @Override
  public String getFtp() {
    return ftp;
  }

  public void setFtp(String ftp) {
    this.ftp = ftp;
  }

  @Override
  public String getSwissprotFasta() {
    return swissprotFasta;
  }

  public void setSwissprotFasta(String swissprotFasta) {
    this.swissprotFasta = swissprotFasta;
  }

  @Override
  public String getTremblFasta() {
    return tremblFasta;
  }

  public void setTremblFasta(String tremblFasta) {
    this.tremblFasta = tremblFasta;
  }

  @Override
  public String getIdmapping() {
    return idmapping;
  }

  public void setIdmapping(String idmapping) {
    this.idmapping = idmapping;
  }

  @Override
  public String getTaxonomyMappingName() {
    return taxonomyMappingName;
  }

  public void setTaxonomyMappingName(String taxonomyMappingName) {
    this.taxonomyMappingName = taxonomyMappingName;
  }
}
