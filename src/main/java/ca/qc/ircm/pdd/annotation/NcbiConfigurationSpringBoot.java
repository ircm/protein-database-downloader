/*
 * Copyright (c) 2006 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.pdd.annotation;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = NcbiConfigurationSpringBoot.PREFIX)
public class NcbiConfigurationSpringBoot implements NcbiConfiguration {
  public static class Refseq {
    private String releaseNumber;

    public String getReleaseNumber() {
      return releaseNumber;
    }

    public void setReleaseNumber(String releaseNumber) {
      this.releaseNumber = releaseNumber;
    }
  }

  public static final String PREFIX = "ncbi";
  private String ftp;
  private String taxonomy;
  private String taxonomyNodes;
  private String eutils;
  private int maxIdsPerRequest;
  private Refseq refseq;

  @Override
  public String getRefseqReleaseNumber() {
    return refseq.getReleaseNumber();
  }

  @Override
  public String getEutils() {
    return eutils;
  }

  public void setEutils(String eutils) {
    this.eutils = eutils;
  }

  @Override
  public String getTaxonomy() {
    return taxonomy;
  }

  public void setTaxonomy(String taxonomy) {
    this.taxonomy = taxonomy;
  }

  @Override
  public String getTaxonomyNodes() {
    return taxonomyNodes;
  }

  public void setTaxonomyNodes(String taxonomyNodes) {
    this.taxonomyNodes = taxonomyNodes;
  }

  @Override
  public String getFtp() {
    return ftp;
  }

  public void setFtp(String ftp) {
    this.ftp = ftp;
  }

  @Override
  public int getMaxIdsPerRequest() {
    return maxIdsPerRequest;
  }

  public void setMaxIdsPerRequest(int maxIdsPerRequest) {
    this.maxIdsPerRequest = maxIdsPerRequest;
  }

  public Refseq getRefseq() {
    return refseq;
  }

  public void setRefseq(Refseq refseq) {
    this.refseq = refseq;
  }
}
