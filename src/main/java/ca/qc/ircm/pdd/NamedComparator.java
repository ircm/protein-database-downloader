/*
 * Copyright (c) 2006 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.pdd;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Comparator for {@link Named}.
 */
public class NamedComparator implements Comparator<Named>, Serializable {
  private static final long serialVersionUID = -157683438900880530L;
  private final Comparator<String> nameComparator;

  public NamedComparator() {
    this.nameComparator = new StringComparator(true);
  }

  public NamedComparator(Comparator<String> nameComparator) {
    this.nameComparator = nameComparator;
  }

  @Override
  public int compare(Named o1, Named o2) {
    if (o1 != null && o1.getName() != null && o2 != null && o2.getName() != null) {
      return nameComparator.compare(o1.getName(), o2.getName());
    } else if (o1 != null && o1.getName() != null) {
      return -1;
    } else if (o2 != null && o2.getName() != null) {
      return 1;
    } else {
      return 0;
    }
  }
}
