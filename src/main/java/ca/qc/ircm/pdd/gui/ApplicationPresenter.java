/*
 * Copyright (c) 2006 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.pdd.gui;

import ca.qc.ircm.pdd.download.gui.DownloadPresenter;
import ca.qc.ircm.pdd.download.gui.DownloadView;
import ca.qc.ircm.pdd.organism.gui.ManageOrganismsPresenter;
import ca.qc.ircm.pdd.organism.gui.ManageOrganismsView;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.Tab;
import javafx.scene.layout.Region;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Main application controller.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ApplicationPresenter {
  @FXML
  private ResourceBundle resources;
  @FXML
  private Region layout;
  @FXML
  private Tab downloadTab;
  @FXML
  private Tab organismsTab;

  @FXML
  private void initialize() {
    layout.setPrefHeight(Integer.parseInt(resources.getString("height")));
    layout.setPrefWidth(Integer.parseInt(resources.getString("width")));

    DownloadView downloadView = new DownloadView();
    DownloadPresenter downloadPresenter = (DownloadPresenter) downloadView.getPresenter();
    ManageOrganismsView manageOrganismsView = new ManageOrganismsView();
    ManageOrganismsPresenter manageOrganismsPresenter =
        (ManageOrganismsPresenter) manageOrganismsView.getPresenter();
    downloadPresenter.organismsProperty()
        .bindBidirectional(manageOrganismsPresenter.organismsProperty());
    downloadTab.setContent(downloadView.getView());
    organismsTab.setContent(manageOrganismsView.getView());
  }
}
