/*
 * Copyright (c) 2006 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.pdd.gui;

import java.util.concurrent.ExecutorService;
import java.util.function.Consumer;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.stage.Window;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Starts task and link it to a progress dialog.
 */
@Component
public class TaskStarter {
  public interface TaskStarterBuilder<T extends Task<V>, V> {
    public TaskStarterBuilder<T, V> apply(Consumer<T> consumer);

    public TaskStarterBuilder<T, V> onSuccess(Consumer<V> success);

    public TaskStarterBuilder<T, V> onFail(Consumer<Throwable> fail);

    public void start();
  }

  private static final Logger logger = LoggerFactory.getLogger(TaskStarter.class);
  @Autowired
  private ExecutorService executorService;

  protected TaskStarter() {
  }

  protected TaskStarter(ExecutorService executorService) {
    this.executorService = executorService;
  }

  public <T extends Task<V>, V> TaskStarterBuilder<T, V> build(Window window, T task) {
    return new TaskStarterBuilderImpl<>(window, task);
  }

  private class TaskStarterBuilderImpl<T extends Task<V>, V> implements TaskStarterBuilder<T, V> {
    private Window window;
    private T task;

    private TaskStarterBuilderImpl(Window window, T task) {
      this.window = window;
      this.task = task;
    }

    @Override
    public TaskStarterBuilder<T, V> apply(Consumer<T> consumer) {
      consumer.accept(task);
      return this;
    }

    @Override
    public TaskStarterBuilder<T, V> onSuccess(Consumer<V> success) {
      task.setOnSucceeded(e -> success.accept(task.getValue()));
      return this;
    }

    @Override
    public TaskStarterBuilder<T, V> onFail(Consumer<Throwable> fail) {
      task.addEventHandler(WorkerStateEvent.WORKER_STATE_FAILED,
          e -> logger.error("failed", task.getException()));
      task.setOnFailed(v -> fail.accept(task.getException()));
      return this;
    }

    @Override
    public void start() {
      new ProgressDialog(window, task);
      executorService.submit(task);
    }
  }
}
